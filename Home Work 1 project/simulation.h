#pragma once

#include <vector>
#include <set>
#include "rigidBody.h"
#include "utils.h"
#include "camera.h"
#include "contact.h"

#define EPSILON 0.5f //cofficient of restitution


class simulation
{
public:
	simulation(void);
	~simulation(void);

	
	/* setter fucntions*/
	void setCamera(camera *inCamera);
	void addBody(rigidBody *inBody);

	/*simulation functions*/
	void simulateOld(double dt);
  //Below function is based on SIG 2003 paper "Nonconvex Rigid Bodies with Stacking"
  void simulateOptimized(double dt);
	
	void updatePositionRotation(double dt, rigidBody *body);

  //list of rigid bodies to simulate
	std::vector<rigidBody*> bodies;


private:
///Functions
	bool resolveCollisions(std::vector<Contact*> &contacts, bool isUseTmp);
	void handleCollision(Contact* c);
  //Adding epsilon as parameter, can useful for resting contact
  glm::vec3 handleCollisionWithFriction(Contact* c, float epsilon, int pointIndex, bool isUseTmp);

  //contact
  void resolveContact(std::vector<Contact*> &contacts, float coeffOfRest, bool isLastSolvingStep);

  //New updating velocities
  void collision(float dt);
  void updateVelocity(float dt);
  void contact(float dt);
  void updatePosition(float dt);

  //UTILS functionalities
  void createDAGGraph(std::set<rigidBody *> &nodes, std::vector< std::pair<rigidBody*, rigidBody*> > &graphEdges, float dt, std::vector<Contact*> &refContact);

  void createDAGGraph2(std::set<rigidBody *> &nodes, std::vector< std::pair<rigidBody*, rigidBody*> > &graphEdges, float dt, std::vector<Contact*> &refContact);

  void distroyContacts(std::vector<Contact*> &c);
  //This function (as described in Stanford 2003 paper Point 6, Paragraph 3, And we marked it as 2nd point) Use to reduce inherit bias in ordering.
  void swapTwoBodiesRandomly();


//My verison of calculating impulse for a contact 
  glm::vec3 myHandleCollisionWithFriction(Contact* c, float epsilon, int pointIndex, bool isUseTmp);

///Data members
	//list of rigid bodies to simulate
	//std::vector<rigidBody*> bodies; //shifted to up
	//current camera
	camera* currentCamera;
};

