#pragma once
#include <GL/glew.h>

class textureUtils
{
public:
  textureUtils(void);
  ~textureUtils(void);
  static void loadBMPTexture(const char* fileName, GLubyte** imageRaw, int *width, int *height); 
};

