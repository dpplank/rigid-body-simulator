#pragma once
#include<GL/glew.h>
#include<glm/glm.hpp>
#include<glm/gtc/type_ptr.hpp>
#include<glm/gtc/matrix_transform.hpp>

class shader
{
public:
  enum drawType { LINE, TRIANGLE };
	// below is calls use to draw any object just use this
	// class to draw the object you like
	class DrawObject {

	public:
		virtual GLuint idxCount() = 0;
    virtual GLboolean bindIdx() = 0;
		virtual GLboolean bindPos() = 0;
		virtual GLboolean bindNor() = 0;
    virtual GLboolean bindUV() = 0;
    virtual GLboolean bindTexture() = 0;
	};

	//variable store address of current shader program
	GLuint program;
	
	//variable to store address different variables in our shaders
	GLint vPos;
	GLint vNor;
  GLint vUV;
	
	//uniform variables
	GLint unifModel;
	GLint unifInvModel;
	GLint unifViewProj;
	GLint unifColor;
  GLint unifTextureID1;

public:
	shader();
	void create(const char *vShader, const char *fShader);

	void loadModelMat(glm::mat4 modelMat);
	void loadViewProj(glm::mat4 viewProjMat);
	void loadUnifColor(glm::vec4 inColor);
	
  void draw(DrawObject &d, shader::drawType inDrawType);
	void useThis();
	~shader(void);
};

