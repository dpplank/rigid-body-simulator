#pragma once
#include <vector>
#include "simulation.h"
#include "sphere.h"
#include "ground.h"

class collisionDetection
{
public:
	collisionDetection(void);
	~collisionDetection(void);
  //ANY OPTIMAL DATA STRUCTUCTURE SUCH AS KD-TREE Can be use at discoverCollistion
	bool static discoverCollision(const std::vector<rigidBody*> &bodies, std::vector<Contact*> &contacts, bool isForCollision, bool isUseTmp);//isForCollision:- true-For collision, false- for contact

	bool static collidingContact(Contact *c, int pointIndex, bool isUseTmp);

  bool static seperatingContact(Contact *c, int pointIndex, bool isUseTmp);

  bool static detectCollision(rigidBody *r1, rigidBody *r2, std::vector<Contact*>& contacts,bool isForCollision, bool isUseTmp);

private:

	static bool sphereSphere(sphere *s1, sphere *s2, std::vector<Contact*> &contacts, bool isUseTmp);
	//inOrder, is for only so that we will get appropirate Normal, which is pointing form body B to body A
	//We are doing this because we are store object a as r1 and b as r2 always in discoverCollision routien
	static bool sphereGround1(sphere *s, ground *g, std::vector<Contact*> &contacts, bool inOrder, bool isUseTmp);
  static bool sphereGround(sphere *s, ground *g, std::vector<Contact*> &contacts, bool inOrder, bool isUseTmp);

  static bool cubeGround(cube* c, ground *g, std::vector<Contact*> &contacts, bool isForCollision, bool isUseTmp);
  static bool cubeSphere(cube *c, sphere *s, std::vector<Contact*> &contacts, bool isUseTmp); //This is ideal fucntion for all previous collision detection functions
  static bool cubeCube(cube *c1, cube *c2, std::vector<Contact*> &contacts, bool isForCollision, bool isUseTmp);

  //Miscellenious methods
  //It will project only outside points to wall of cube, just by checking min,max 
  // cube side value for each axis
  //NOTE: It projects only outside points and ideal for sphere-box intersection
  static void closestPoint(cube *c, glm::vec4 p, glm::vec4 &pRes, bool isUseTmp); 
  //This method use for SAT theorem, for box collision detection, it return whether two boxes
  //are colliding on given axis, if yes then it return overlapping distance also
  bool static checkBoxAxisOverlap(glm::vec3 axis, cube *c1, cube *c2, float &overlap, bool &isC1BackSide, bool isUseTmp);
  std::pair<float, float> static getBoxProjection(cube *c, glm::vec3 axis, bool isUseTmp);
  void static getPossibleBestPoint(cube *c1, cube *c2, std::vector<glm::vec4> &p, int axisIndex, bool isC1BackSide, glm::vec3 collisionNormal, bool isForCollision, bool isUseTmp);

  void static getEdgeEdgePoint(cube *c, const std::vector<edge> &e, std::vector<glm::vec4> &p, glm::vec3 collisionNormal, bool isForCollision, bool isUseTmp);

  static bool pointInBox(cube *c, glm::vec4 p, bool isUseTmp);
  
  //Given line made of p1 and p2, need to test whether this line intersect in cube or not. Via "intersectPoint" send appropriate point (see function for more details). 
  static bool lineInBox(cube* c, glm::vec4 p1, glm::vec4 p2, glm::vec3 collisionNormal, glm::vec4 &intersectPoint, bool isUseTmp);

};

