#include "rigidBody.h"
#include "utils.h"


rigidBody::rigidBody(double inMass, double inCoeffFriction) {
	if(inMass < 0 ) { //For infinite mass
		massInv = 0;
	} else {
		massInv = 1/inMass;
	}

	R = glm::mat3(1);
	omega = glm::vec3(0, 0, 0);
	bodyType = UNKNOWN;
  
  coeffFriction = inCoeffFriction;

  level=-1;
  isInfiniteMass=false;
}


rigidBody::~rigidBody(void) {

}

void rigidBody::setColor(glm::vec4 inColor) {
  color = inColor;
}

void rigidBody::setRotation(glm::vec3 axis, float angleInDegree) {
  float a = axis[0], b = axis[1], c= axis[2];
  float theta = angleInDegree * PI / 180; //angle In radian
  R = glm::mat3(
    a*a + (1 - a*a)*cos(theta),         a*b*(1-cos(theta)) - c*sin(theta),  a*c*(1-cos(theta)) + b*sin(theta),
    a*b*(1-cos(theta)) + c*sin(theta),  b*b + (1 - b*b)*cos(theta),         b*c*(1-cos(theta)) - a*sin(theta),
    a*c*(1-cos(theta)) - b*sin(theta),  b*c*(1-cos(theta)) + a*sin(theta),  c*c + (1 - c*c)*cos(theta) 
    );

  //Because glm stores Column major matirix
  R = glm::transpose(R);
}

void rigidBody::addToQueueMomentum(glm::vec3 inJ, glm::vec3 inR, bool isUseTmp) {
  queueVelChange += getMassInv() * glm::vec4(inJ,0); 

  queueOmegaChange += getCurrentInertiaInv(true) * glm::cross(inR, inJ);

}

void rigidBody::setOmegaRad(glm::vec3 inOmega) {
  //converting inOmega into degree for intenal storage
  omega = inOmega * 180.0f/PI;
}

void rigidBody::applyQueuedMomentum() {
  v += queueVelChange;
  //Changin in omega is in Radiun, so converting it to Degree, as underline representing
  omega += queueOmegaChange * 180.0f/PI;

  queueVelChange = glm::vec4(0);
  queueOmegaChange = glm::vec3(0);
}

glm::vec4 rigidBody::getColor() {
  return color;
}

float rigidBody::getMass() {
	return 1/massInv;
}

float rigidBody::getMassInv() {
	return massInv;
}

rigidBody::BodyTypes rigidBody::getType() {
	return bodyType;
}

//glm::mat4 rigidBody::getModelMatrix() {
//  return glm::mat4(1);
//
//}

glm::mat3 rigidBody::getRotationMatrix(bool isUseTmp) {
  if(isUseTmp) {
    return RTmp;
  } else {
	  return R;
  }
}

glm::mat4 rigidBody::giveCurrentPosMatrix(bool isUseTmp) {
  if (isUseTmp) {
    return glm::mat4( 1		    , 0		    , 0		    , 0,
		          			  0		    , 1		    , 0		    , 0,
					            0		    , 0		    , 1		    , 0,
					            xTmp[0]	, xTmp[1]	, xTmp[2]	, 1);
 
  } else {
	  return glm::mat4( 1		, 0		, 0		, 0,
		          			  0		, 1		, 0		, 0,
					            0		, 0		, 1		, 0,
					          x[0]	, x[1]	, x[2]	, 1);
  }
}

glm::mat3 rigidBody::getCurrentInertia(bool isUseTmp) {
  if(isUseTmp) {
    return RTmp * Ibody * glm::transpose(R);
  } else {
    return R * Ibody  * glm::transpose(R);
  }
}

glm::mat3 rigidBody::getCurrentInertiaInv(bool isUseTmp) {
  if(isUseTmp) {
    return RTmp * Ibodyinv * glm::transpose(RTmp);
  } else {
    return R * Ibodyinv * glm::transpose(R);
  }
}

glm::vec3 rigidBody::getOmegaRad() {
  return (PI/180.0f)*omega;
}

void rigidBody::applyForceTorque(glm::vec3 inForce, glm::vec3 position) {
	force += glm::vec4(inForce,0);
	torque += glm::cross(position, inForce);
}

void rigidBody::applyImpulse(glm::vec3 inJ, glm::vec3 position, bool isUseTmp) {
  v = v + glm::vec4(inJ,0) * getMassInv();
  setOmegaRad( getOmegaRad() + getCurrentInertiaInv(isUseTmp) * glm::cross(position, inJ) );
}

void rigidBody::applyLinearImpulse(glm::vec3 inJ) {
  v = v + glm::vec4(inJ,0) * getMassInv();
}


void rigidBody::clearContacts() {
  contacts.clear();
}