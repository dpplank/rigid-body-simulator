#include<stdio.h>
#define GLM_SWIZZLE //It need to define before any GLM inclusion for
					//for swizzle like GLSL ex if vec4 r; then r.xyx; is valid type of things

#include <iostream>
#include "camera.h"
#include <glm/gtc/matrix_transform.hpp>


using namespace glm;

#define PI 3.141

using namespace std;

camera::camera() {
    ref = vec4(0,0,0,1);
    theta = 0; phi = 0;
    zoom = 5;//10;

    fovy = 60;//45;
    nearP = 1;//.1;
    farP = 200;
    width = 1;
    height = 1;

    printf("11 hari hari ");
}

camera::camera(float inWidth, float inHeight) {
    theta = 0; phi = 0;
    zoom = 5;
    nearP = 0.1;
    farP = 100;


    width = inWidth;
    height = inHeight;
}

void camera::setWidthHeight(float inWidth, float inHeight) {
  width = inWidth;
  height = inHeight;
}

mat4 camera::view() {
    //***// angle in radian
    float thetaRad = theta * PI / 180; // rotation measured from Y-Z plan about Y-axis
    float phiRad = phi * PI / 180; // rotation measured from Z-X plan about X-axis


    //***// calculation of position of camera
    vec4 position (zoom * cos(phiRad) * sin(thetaRad) ,
                   zoom * sin(phiRad), //Move up as "phi" increase
                   zoom * cos(phiRad) * cos(thetaRad),
                   1);

    //***// calculate 3 vector of camera axis or of plane of projection
    // [-Z axis]
	vec4 secAxis = glm::rotate(mat4(1),theta,vec3(0,1,0))*vec4(0,0,-1,0);
	secAxis = glm::rotate(mat4(1),90.0f,vec3(0,1,0)) * secAxis;

	vec4 Zaxis = glm::rotate(mat4(1),phi,vec3(secAxis[0],secAxis[1],secAxis[2]))*glm::rotate(mat4(1),theta, vec3(0,1,0)) * vec4(0,0,-1, 0);

    // [Y axis]
    //vec4 Yaxis = glm::rotate(mat4(1), phi, vec3(secAxis[0],secAxis[1],secAxis[2])) * vec4(0,1,0,0);
	vec4 Yaxis = glm::rotate(mat4(1), phi, vec3(secAxis.xyz)) * vec4(0,1,0,0); //BECAUSE OF GLM_SWIZZLE from documentation

    // [X axis]
   vec4 Xaxis = vec4(cross(vec3(Zaxis[0], Zaxis[1], Zaxis[2]), vec3(Yaxis[0], Yaxis[1], Yaxis[2])), 0);
   Xaxis /= length(Xaxis);



    //***// Creating resultant matrix
    mat4 rot (Xaxis,
              Yaxis,
              -1.0f*Zaxis, //placing Z axis of camera at -Z axis
              vec4(0,0,0,1));
	rot = glm::transpose(rot);
   mat4 trans (vec4(1, 0, 0, -(position[0]+ref[0])),
               vec4(0, 1, 0, -(position[1]+ref[1])),
               vec4(0, 0, 1, -(position[2]+ref[2])),
               vec4(0, 0, 0, 1));
   //std::cout<<"X=: "<<ref[0]<<"Y=: "<<ref[1]<<"Z=: "<<ref[2]<<std::endl;
   trans = glm::transpose(trans);
    mat4 out = rot * trans;
	//cout<<"zoom: "<<zoom<<endl;

    return out;
}

mat4 camera::perspective() {
    float aspect = width/height;
    float top = nearP * tan((fovy/2) * PI / 180);
    float bottom = -top;
    float right = top*aspect;
    float left = -right;

   /* mat4 out = mat4( vec24(2*nearP/(right-left), 0             , (right+left)/(right-left)             ,0 ),
                     vec4(0             , 2*nearP/(top-bottom), (top+bottom)/(top-bottom)             ,0 ),
                     vec4(0             , 0             , (nearP)/(farP-nearP), -nearP*farP/(farP-nearP)),
                     vec4(0             , 0             , -1             , 0)
                    ); */
    mat4 out = mat4( 2*nearP/(right-left) , 0                   , (right+left)/(right-left) , 0                         ,
                     0                  , 2*nearP/(top-bottom), (top+bottom)/(top-bottom)   , 0                         ,
                     0                  , 0                   , (nearP)/(farP-nearP)        , -nearP*farP/(farP-nearP)  ,
                     0                  , 0                   , -1 /*b'cos nearP is +ve*/   , 0
                    );

    out = glm::transpose(out);
	
//    double theta=fovy/2;
//        double d = 1/tan(theta*PI/180);
//        double A=-(farP+nearP)/(farP-nearP);
//        double B=-2*farP*nearP/(farP-nearP);

//        mat4 out=mat4(vec4(d/aspect,0 ,0 ,0),
//                      vec4(0       ,d ,0 ,0),
//                      vec4(0       ,0 ,A ,B),
//                      vec4(0       ,0 ,-1,0));
    //out = (out);


//    cout<<"projection matrix: "<<out;

    return out;
}

mat4 camera::orthogonal() {
    float aspect = width/height;
    float top = nearP * tan((fovy/2) * PI/180);
    float bottom = -top;
    float right = top*aspect;
    float left = -right;

    float nearPCoord = -nearP;//-nearP;
    float farPCoord = -farP;//-farP;

    //cout<<"fovy: "<<fovy<<endl;

    //while initializing with vec4, we don't need to transpose because, it initialize it with row major fasion
    mat4 out ( vec4( 2/(right-left), 0             , 0                       , -1*(right+left)/(right-left) ),
               vec4( 0             , 2/(top-bottom), 0                       , -1*(top+bottom)/(top-bottom)),
               vec4( 0             , 0             , 2/(farPCoord-nearPCoord), -1*(farPCoord+nearPCoord)/(farPCoord-nearPCoord)),
               vec4( 0             , 0             , 0                       , 1)
               );
	//out = glm::transpose(out);
    return out;
}

vec4 camera::getPosition() {
    float thetaRad = theta * PI / 180; // rotation measured from Y-Z plan about Y-axis
    float phiRad = phi * PI / 180; // rotation measured from Z-X plan about X-axis

    vec4 position (zoom * cos(phiRad)* sin(thetaRad),
                   zoom * sin(phiRad),
                   zoom * cos(phiRad) * cos(thetaRad),
                   1);
    return position;
}

vec4 camera::getRight(){
    vec4 right = rotate(mat4(1),theta, vec3(0,1,0)) * vec4(1,0,0,0); //rotating x axis
    return right;
}

vec4 camera::getUp() {

    vec4 rotAbout = rotate(mat4(1), theta, vec3(0,1,0)) * vec4(1,0,0,0);

    vec4 up = rotate(mat4(1), phi, vec3(-rotAbout[0], -rotAbout[1], -rotAbout[2])) * vec4(0,1,0,0);

    return up;
}

glm::vec4 camera::getLookAtDir() {
  return glm::normalize(ref - getPosition());
}

camera::~camera()
{

}

