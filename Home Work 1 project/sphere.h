#pragma once
#include "shader.h"
#include "rigidBody.h"

class sphere : public shader::DrawObject, public rigidBody
{

public:
	sphere(double inMass, double inCoeffFrictoin = 0.5);
	void create();
	void bindVAO();
	virtual ~sphere(void);

	//Derived functions from DrawObject
	virtual GLuint idxCount();
	virtual GLboolean bindIdx();
	virtual GLboolean bindPos();
	virtual GLboolean bindNor();
  virtual GLboolean bindUV();
  virtual GLboolean bindTexture();

	//Setter functions
	void setScale(float scaleX,  float scaleY, float scaleZ);
	glm::mat4 getScaleMatrix();
  void setRadius (float inRadius);
  void setTexture(const char* fileName);

  //Getter
	float getRadius();
	glm::vec4 getCenter(bool isUseTmp);

	virtual glm::mat4 getModelMatrix(bool isUseTmp);

private:
	GLuint count;
	GLuint bufIdx;
	GLuint bufPos;
	GLuint bufNor;
  GLuint bufUV;

  GLuint textureID;
	glm::vec3 _scale; //non rigid body component

};

