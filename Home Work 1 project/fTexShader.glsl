//Hare Ram Hare Ram
//Ram Ram Hare Hare

//#version 450

uniform mat4 uModelMat;
uniform mat4 uInvTranModelMat; //For normal tranformation
uniform mat4 uViewProjMat;
uniform vec4 uColor;
//Texture sampler
uniform sampler2D texture1;

//in stream data
in vec4 fsNormal;
in vec4 vColor;
in vec2 fsUV;

//out stream data
out vec4 outColor;

void main() {
  
  //outColor = vColor;
  outColor = texture2D(texture1, fsUV);

}