#define GLM_SWIZZLE 
#include "simulation.h"
#include <iostream>
#include <numeric> //for std::accumulate
#include "collisionDetection.h"
#include <GL/freeglut.h>

#define DEBUG 1
#define FIRST_POINT_INDEX 0

simulation::simulation(void) : currentCamera(nullptr){
}


simulation::~simulation(void){

}

void simulation::setCamera(camera *inCamera) {
	currentCamera = inCamera;
}

void simulation::addBody(rigidBody *inBody) {
	bodies.push_back(inBody);
}

void simulation::simulateOld(double dt) {
	
	//store all bodies

	//update bodies one time step
	//Updating position, rotation, velocity and rot. velocity
	for(int i=0; i<bodies.size(); i++) {
		updatePositionRotation(dt, bodies[i]);
	}
  
  //PROBLEM: with this rewinding simulation with -dt is, we are getting point at +dt time,
  //and after rewinding our simulation, we are getting incorrect value of point of intersection
	//for(int i=0; i<bodies.size(); i++) {
	//	updatePositionRotation(-dt, bodies[i]);
	//}

	std::vector<Contact*> contacts;

	bool result = collisionDetection::discoverCollision(bodies, contacts, true, false);
	if(result) {
		//revert update to all bodies
		/*for(int i=0; i<bodies.size(); i++) {
			updatePositionRotation(-dt, bodies[i]);
		}*/
		resolveCollisions(contacts, false);
	}

 // for(int i=0; i<bodies.size(); i++) {
	//	updatePositionRotation(dt, bodies[i]);
	//}
	//system("pause");

	//Check for collision
		//If found collisiont, revert all bodies to previous time
		
		//find time of collision

		//get 'Contact' structure filled with necessary 
		
		//Calculate Impulse and apply on both bodies
		
		//continue for all bodies
  
  distroyContacts(contacts);

	if(currentCamera);
		//currentCamera->ref = bodies[5]->x;

}


void simulation::simulateOptimized(double dt) {
  // based on SIGGRAPH 2003 paper "Nonconvex Rigid Bodies with Stacking"'s section no. 6
  //Resolve Collision
  collision(dt);
  //Update velocity
  updateVelocity(dt);

  //Resolve Contact
  contact(dt);

  //Update position
  updatePosition(dt);

}

void simulation::collision(float dt) {
  //(See below swap fuction decription in Header to know its functioning)
  //BUG:: With this function on, our body srinking down in floor as time passes. Seems some issue in Contact
  //swapTwoBodiesRandomly();

  for(int itr=0; itr<5; itr++) {
    //get pridected position using P = P + (v + f*dt)*dt; 
    for(int i=0; i<bodies.size(); i++) {
      //Temperorily updating position and rotation
      bodies[i]->xTmp = bodies[i]->x + dt * (bodies[i]->v + dt * bodies[i]->force*bodies[i]->getMassInv());
      //TODO::: ALSO CONSIDETING TORQUE IN OMEGA, JUST LIKE ABOVE FOR VELOCITY
      float omegaAbs = glm::length(bodies[i]->omega); 
      if( omegaAbs > 0.01) {
        glm::mat3 ITmp  =		bodies[i]->R * bodies[i]->Ibody * glm::transpose(bodies[i]->R);
	      glm::mat3 IInvTmp =	bodies[i]->R * bodies[i]->Ibodyinv * glm::transpose(bodies[i]->R);

	      glm::vec3 omegaTmp = bodies[i]->omega;// + (180/PI)*(float)dt * IInvTmp * (bodies[i]->torque - utils::giveVecStar(bodies[i]->omega*PI/180.0f) * (ITmp * bodies[i]->omega*PI/180.0f));
        //PERFORMANCE TODO:: below we're doing glm::lenth 2 times, instead we can use above "omegaAbs" for that purpose
        bodies[i]->RTmp = utils::giveRodrignesRot(glm::length(omegaTmp * (float)dt), omegaTmp/glm::length(omegaTmp)) * bodies[i]->R; 
      }
	  }
  
    std::vector<Contact*> contacts;

    //Get all contact points for collision with new tmp position
	  bool result = collisionDetection::discoverCollision(bodies, contacts, true, true);
  
    //Resolve all the contact with current velocities
    if(result) {      
      bool isAnyCollision = resolveCollisions(contacts, true);
      if(!isAnyCollision){
        break;
      }
    } else {
      break;
    }

    distroyContacts(contacts);
  }
}

void simulation::updateVelocity(float dt) {
  for(int i=0; i<bodies.size(); i++) {
    //Linear velocity
    bodies[i]->v = bodies[i]->v + (bodies[i]->force*bodies[i]->getMassInv()) * (float)dt;

    //Rotational velocity
    bodies[i]->I =		bodies[i]->R * bodies[i]->Ibody * glm::transpose(bodies[i]->R);
	  bodies[i]->Iinv =	bodies[i]->R * bodies[i]->Ibodyinv * glm::transpose(bodies[i]->R);
    glm::vec3 omegaChange = (float)dt * bodies[i]->Iinv * (bodies[i]->torque - utils::giveVecStar(bodies[i]->getOmegaRad()) * (bodies[i]->I * bodies[i]->getOmegaRad()));
    float lenthOmega = glm::length(omegaChange);
    //bodies[i]->omega = bodies[i]->omega + omegaChange*180.0f/PI;
    if(DEBUG) {
      glm::vec3 angMomentum = bodies[i]->I * bodies[i]->getOmegaRad();
      lenthOmega = glm::length(angMomentum);
      lenthOmega *= 1;
    }
  }
}

void simulation::contact(float dt) {
  for(int i=0; i<2; i++) {
  //Create Directed Acyclic Graph (DAG), (NOTE: IN CASE OF CYLE ALL ELEMENTS IN CYCLE NEED
  //TO DEAL WITH SAME LEVEL NO., NEED TO DEAL WITH THIS LATER, LIKE CIRCULAR DOMINOES).
  std::set<rigidBody *> graphNodes;
  std::vector< std::pair<rigidBody*, rigidBody*> > graphEdges;

  //below variable to keep reference of all contacts, to delete them
  //NOTE: instead of taking below reference, we can make smart pointer for Contacts, Its ideal case here to use them
  std::vector<Contact*> allContacts;

  createDAGGraph2(graphNodes, graphEdges, dt, allContacts);

  //Topological Sorting (TODO LATER: HANDLE CYCLIC GRAPH LIKE ABOVE NOTE CASE)
  std::vector<std::vector<rigidBody *>> sortedGraphNodes; //This is kind of 2D list, it solves the issues of both Dominoes problem and multiple objects at same level problem
  utils::topologicalSort(graphNodes, graphEdges, sortedGraphNodes);


  //declare contactLevels, which contains level wise contacts to solve
  //NOTE: each level "i" in contactLevels contains contact between object of level "i+1" and
  //      level less then or equal "i+1"
  std::vector< std::vector<Contact*> > contactLevels;

  //loop through sortedGraphNodes, but start with level = 1, becasue level 0 represents static objects
  for(int i=1; i<sortedGraphNodes.size(); i++) {
    //Declare contact list for current level
    std::vector<Contact *> levelContact;
    //loop through each element of current level
    for(int j=0; j<sortedGraphNodes[i].size(); j++) {
      //declare a vector of contact which represents all contact at current or below current object
      std::vector<Contact*> cList;
      //loop through each contact in current object, To get contact with current or below level object
      for(int k=0; k<sortedGraphNodes[i][j]->contacts.size(); k++) {
        //Check other object has same or below level 
        Contact * c = sortedGraphNodes[i][j]->contacts[k];
        rigidBody *a = sortedGraphNodes[i][j];
        rigidBody *b = (c->a == a ? c->b : c->a);
        if(b->level <= a->level) {
          //if yes then store that in above contact list
          levelContact.push_back(c);
        }
      }
      ////resolve contact with "c" contacts and 0 epsilon
      //resolveContact(cList, 0/*i/(float)10-1*/);
    }
    //Push current levelContact in list of all contactsLevels
    contactLevels.push_back(levelContact);
  }


  //Do no. of times contact resolution
  for(int itr=0; itr<2/*10*/; itr++) {
    bool hasAnyContact=false;

    //loop through each levelContacts
    for(int i=0; i<contactLevels.size(); i++) {
      //NOTE: for case such as "circular Dominoes" case we have to repeat below loop number of times
      //resolve all contacts at this level
    for(int j=0; j<2; j++) {
      resolveContact(contactLevels[i], 0-(2 - itr - 1)/2.0, false);
      hasAnyContact=true;
    }
    }

    //If we dont has any thing why just loop 5 times
    if(!hasAnyContact) break;
  }

  if(DEBUG) {
    if(contactLevels.size()>1) {
      int i=0;
      i *=2;
    }
  }

  ///LAST CONTACT AND SHOCK PROPOGATION(FREEZE STEP)
  //  One last time contact resolution and then FREEZE current object
  //Iterate through all contactLevels
  for(int i=0; i<contactLevels.size(); i++) {
    //resolve current level contacts
    for(int itr=1; itr<=2; itr++) {
      resolveContact(contactLevels[i], 0, false);//-(10-itr)/10.0f );
      //::NOTE:: Below code making the top of the tower blow avaw when anything hit
      //resolveContact(contactLevels[i], 0, true);//-(10-itr)/10.0f );
    }
    
    //Set infinite Mass at current level
    for(int j=0; j<sortedGraphNodes[i+1].size(); j++) {
      //FReze all objects at this level
      //Note: first index is i+1, because, one object in contact of i index in contactLevels
      //      has i+1 index in sortedGraphNodes
      sortedGraphNodes[i+1][j]->isInfiniteMass = true;
      rigidBody *b = sortedGraphNodes[i+1][j];
      ///Why AcceptableThershold: because, it is making (by now) perfect match. In scene "edgeTestCube" where cube little stop in mid air (while on one edge) perfectly go down instead of hanging in air with "0.008", it hangs in air
      const float acceptableThershold = 0.007;
      if(glm::dot(b->v,b->v)<acceptableThershold) {
        b->v = glm::vec4(0);
        if(glm::dot(b->omega,b->omega)<1) b->omega = glm::vec3(0);
      }
    }
  }

  //set isInfiniteMass to false
  for(auto itr=graphNodes.begin(); itr!= graphNodes.end(); itr++) {
    (*itr)->isInfiniteMass = false;

  }

  
  //Destroy Contacts
  distroyContacts(allContacts);

  //Clear all contacts in each body
  for(auto i=graphNodes.begin(); i!=graphNodes.end(); i++) {
    (*i)->clearContacts();
  }

  }
  //for(int itr=0; itr<25; itr++) {
  //  for(int i=0; i<bodies.size(); i++) {
  //    //Temperorily updating position and rotation
  //    bodies[i]->xTmp = bodies[i]->x + dt * (bodies[i]->v);
  //    //(No need later found)TODO::: ALSO CONSIDETING TORQUE IN OMEGA, JUST LIKE ABOVE FOR VELOCITY
  //    float omegaAbs = glm::length(bodies[i]->omega); 
  //    if( omegaAbs > 0.01) {	   
  //      bodies[i]->RTmp = giveRodrignesRot(glm::length(bodies[i]->omega * (float)dt), bodies[i]->omega/glm::length(bodies[i]->omega)) * bodies[i]->R; 
  //    }
	 // }
  //  std::vector<Contact*> contacts;
	 // bool result = collisionDetection::discoverCollision(bodies, contacts, true);
  //
  //  if(result) {
  //    for(int i=1; i<=1/*0*/; i++) {
  //      resolveContact(contacts, 0/*i/(float)10-1*/);
  //    }
  //  } else {
  //    break;
  //  }
  //}


}

void simulation::resolveContact(std::vector<Contact*> &contacts, float coeffRest, bool isLastSolvingStep) {
  for(int i=0; i<contacts.size(); i++) {
    bool isNonZeroImpulse=true;

    if(!isLastSolvingStep) {
      Contact cAverage = *contacts[i];
      glm::vec4 point = std::accumulate(contacts[i]->p.begin(), contacts[i]->p.end(), glm::vec4(0,0,0,0));
      point /= contacts[i]->p.size();
      cAverage.p.push_back(point);
      ////**NOTE*** WHY WE ARE APPLYING CONTACT ON RESTING CONTACTS!!
      if(!collisionDetection::seperatingContact(&cAverage, cAverage.p.size()-1, false) ) {

        glm::vec3 j = handleCollisionWithFriction(&cAverage, coeffRest, cAverage.p.size()-1, false);
        Contact* c = contacts[i];
        glm::vec3 ra, rb;
        ra = glm::vec3((cAverage.p.back() - c->a->x)).xyz;
        rb = glm::vec3((cAverage.p.back() - c->b->x)).xyz;

        if(!(c->a->isInfiniteMass)) {
          // apply j to body a as linear and angular torque
          c->a->addToQueueMomentum(j, ra, false);
          //c->a->applyImpulse(j, ra, false);
        }

        if(!(c->b->isInfiniteMass)) {
          //On body b
          c->b->addToQueueMomentum(-j, rb, false);
          //c->b->applyImpulse(-j, rb, false);
        }
      }

    } else {
      for(int k=0; k<contacts[i]->p.size(); k++) { //Reverse iteration, becouse we might remove some points
        if(!collisionDetection::seperatingContact(contacts[i], k, false) ) 
          {
            glm::vec3 j ;
            Contact cAverage = *contacts[i];

            ////j = handleCollisionWithFriction(contacts[i], coeffRest, k, false);
            //if(k==contacts[i]->p.size()-1)
            //  break;

            //glm::vec4 point = contacts[i]->p[k] + contacts[i]->p[k+1];
            //point /= 2;
            //cAverage.p.push_back(point);
            //j = handleCollisionWithFriction(&cAverage, coeffRest, cAverage.p.size()-1, false);
            j = handleCollisionWithFriction(contacts[i], coeffRest, k, false);

            Contact* c = contacts[i];
            glm::vec3 ra, rb;
            ra = glm::vec3((c->p[k] - c->a->x)).xyz;
            rb = glm::vec3((c->p[k] - c->b->x)).xyz;
            
            if(!(c->a->isInfiniteMass)) {
              // apply j to body a as linear and angular torque
              //c->a->applyLinearImpulse(j);
              c->a->addToQueueMomentum(j, ra, false);
            }

            if(!(c->b->isInfiniteMass)) {
              //On body b
              c->b->addToQueueMomentum(-j, rb, false);
              //c->b->applyLinearImpulse(-j);
            }

            if(DEBUG) {
              glm::vec4 velB = utils::velocityAtPoint(c->b, c->p[k], true);
              glm::vec4 velA = utils::velocityAtPoint(c->a, c->p[k], true);
              float xVel = velB.x + 5;

              glm::vec3 vRel = (velA - velB).xyz; 
              float vReln = glm::dot(glm::vec3(c->n.xyz), vRel);
              vReln*=1;
            }
            if(!DEBUG) {
              glm::mat3 Ka,Kb;
              if(!(c->a->isInfiniteMass)) {
                glm::mat3 IinvA;
                IinvA = c->a->getCurrentInertiaInv(false);
              
                Ka = glm::mat3(1) * c->a->getMassInv() + 
                              glm::transpose(utils::giveVecStar(ra)) * IinvA * utils::giveVecStar(ra);
              }
              if(!(c->b->isInfiniteMass)) {
                glm::mat3 IinvB;// = c->b->getCurrentInertiaInv();
                IinvB = c->b->getCurrentInertiaInv(false);
                Kb = glm::mat3(1) * c->b->getMassInv() +
                                glm::transpose(utils::giveVecStar(rb)) * IinvB * utils::giveVecStar(rb);
              }

              glm::mat3 Kt = Ka + Kb;

          	  glm::vec4 velA = utils::velocityAtPoint(c->a, c->p[k], false);
          	  glm::vec4 velB = utils::velocityAtPoint(c->b, c->p[k], false);
          	  glm::vec3 N = c->n.xyz;

              glm::vec3 vRel = (velA - velB).xyz; 
              float vReln = glm::dot(N, vRel);

              glm::vec3 fvRel = vRel + Kt * j;
              glm::vec4 velChange = - glm::vec4(j,0)*c->b->getMassInv();
              glm::vec4 linearVel = c->b->v - glm::vec4(j,0)*c->b->getMassInv();
              glm::vec3 omegaVel = c->b->omega - c->b->getCurrentInertiaInv(false) * glm::cross(rb, j) * 180.0f/PI;
              glm::vec4 finalVel = linearVel + glm::vec4(glm::cross(omegaVel*PI/180.0f, rb), 0);
    
              glm::vec4 linearVelA = c->a->v + glm::vec4(j,0)*c->a->getMassInv();
              glm::vec3 omegaVelA = c->a->omega + c->a->getCurrentInertiaInv(false) * glm::cross(ra, j) * 180.0f/PI;
              glm::vec4 finalVelA = linearVelA + glm::vec4(glm::cross(omegaVelA*PI/180.0f, ra), 0);

              fvRel *= 1;
            }
          }
        }
  /*      contacts[i]->a->applyQueuedMomentum();
        contacts[i]->b->applyQueuedMomentum();
  */    }
  }

  for(int i=0; i<contacts.size(); i++) {
    contacts[i]->a->applyQueuedMomentum();
    contacts[i]->b->applyQueuedMomentum();
  }

}

void simulation::updatePosition(float dt) {
  for(int i=0; i<bodies.size(); i++) {
    if(!DEBUG) {
      if(glm::dot(bodies[i]->v, bodies[i]->v) < 0.01) {
        bodies[i]->v = glm::vec4(0);
      }
    }
    //Temperorily updating position and rotation
    bodies[i]->x = bodies[i]->x + dt * (bodies[i]->v);

    float omegaAbs = glm::length(bodies[i]->omega);
    if( omegaAbs > 0.01) {
      if(!DEBUG) {
        if(omegaAbs < 0.1) { 
          bodies[i]->omega = glm::vec3(0);
          return ;
        }
      }

      bodies[i]->R = utils::giveRodrignesRot(glm::length(bodies[i]->omega * (float)dt), bodies[i]->omega/glm::length(bodies[i]->omega)) * bodies[i]->R;
    }

	}
  
  if(bodies[1]->x.y < 4) {
    int a=9;
    int b;
    b=a;
  }
  //if(currentCamera)
		//currentCamera->ref = bodies[1]->x;
}

void simulation::updatePositionRotation(double dt, rigidBody *body) {
		//translation Motion
	body->x = body->x + body->v * (float)dt;
	body->v = body->v + (body->force*body->getMassInv()) * (float)dt;
	//c.ref = c1.x;

	//Rotation Motion
	//***PROBLEM:- We have to make all our calculation for 3x3 Matrix instead of 4x4 Matrix and at end while sending rotation Matrix
	//***		   we just have to put (0,0,0,1) at end and converting 3x3 to 4x4 Matrix
	//Debug: std::cout<<"Determinant of Rotation R is: "<<glm::determinant(body->R)<<std::endl;
	//body->R = body->R + (float)dt * utils::giveVecStar(body->omega) * body->R;
  //if(!utils::epsilonCheck(body->getMassInv(), 0)) {
  float omegaAbs = glm::length(body->omega); 
  if( omegaAbs > 0.1) {
   body->R = utils::giveRodrignesRot(glm::length(body->omega * (float)dt), body->omega/glm::length(body->omega)) * body->R;
  }

	body->I =		body->R * body->Ibody * glm::transpose(body->R);
	body->Iinv =	body->R * body->Ibodyinv * glm::transpose(body->R);

  body->setOmegaRad ( body->getOmegaRad() + (float)dt * body->Iinv * (body->torque - utils::giveVecStar(body->getOmegaRad()) * (body->I * body->getOmegaRad())) );

}


bool simulation::resolveCollisions(std::vector<Contact*> &contacts, bool isUseTmp) {
	bool collidingContact=true;
  bool isAnyCollision=false; //This is optimization variable, if there is no any colliding contact, we simply stop Collision loop
  int times = 0;
  while(collidingContact) {
    collidingContact = false;
    times++;
    for(int i=0; i<contacts.size(); i++) {
		  if(collisionDetection::collidingContact(contacts[i], FIRST_POINT_INDEX, isUseTmp) ) {
        //collidingContact = true;
        isAnyCollision = true;

        glm::vec3 j = handleCollisionWithFriction(contacts[i], EPSILON, FIRST_POINT_INDEX, isUseTmp);

        if(DEBUG) {
          int debugVal;
          if(j.y > 0) {
            debugVal = 34;
          }
        }

        Contact* c = contacts[i];
        
        glm::vec3 ra;
        glm::vec3 rb;
        if(isUseTmp) {
          ra = glm::vec3((c->p[FIRST_POINT_INDEX] - c->a->xTmp)).xyz;
          rb = glm::vec3((c->p[FIRST_POINT_INDEX] - c->b->xTmp)).xyz;
        } else {
          ra = glm::vec3((c->p[FIRST_POINT_INDEX] - c->a->x)).xyz;
          rb = glm::vec3((c->p[FIRST_POINT_INDEX] - c->b->x)).xyz;
        }
        // apply j to body a as linear and angular torque
        //On body a
        //c->a->v = c->a->v + glm::vec4(j,0) * c->a->getMassInv();
        //c->a->setOmegaRad( c->a->getOmegaRad() + c->a->getCurrentInertiaInv() * glm::cross(ra, j) );
        c->a->addToQueueMomentum(j, ra, isUseTmp);

        //On body b
        /*c->b->v = c->b->v - glm::vec4(j,0) * c->b->getMassInv();
        glm::vec3 omegaChange = c->b->getCurrentInertiaInv()*glm::cross(rb,j);
        c->b->setOmegaRad( c->b->getOmegaRad() - omegaChange * 180.0f/(PI) );*/
        c->b->addToQueueMomentum(-j, rb, isUseTmp);

        glm::vec4 velB = utils::velocityAtPoint(c->b, c->p[FIRST_POINT_INDEX], isUseTmp);
        float xVel = velB.x + 5;
        //printf("debug, xComponent of V: %f\n", xVel);
        //printf("Omega value of Z: %f\n", c->b->omega.z);
        //printf("Linear Velocity in X: %f\n", c->b->v.x);
		  }
	  }

    //Apply Queued collision
    for(int b=0; b<bodies.size(); b++) {
      bodies[b]->applyQueuedMomentum();
    }
  }

  if(!DEBUG) {
    int debug=3;
    if(times>1){
      debug =times;
    }
  }

  return isAnyCollision;
}

glm::vec3 simulation::handleCollisionWithFriction(Contact* c, float epsilon, int pointIndex, bool isUseTmp) {
	glm::vec4 velA = utils::velocityAtPoint(c->a, c->p[pointIndex], isUseTmp);
	glm::vec4 velB = utils::velocityAtPoint(c->b, c->p[pointIndex], isUseTmp);
	glm::vec3 N = c->n.xyz;
  //ra and rb are just vectors so 4th value always zero
  glm::vec3 ra;
  glm::vec3 rb;
  if(isUseTmp) {
    ra = glm::vec3((c->p[pointIndex] - c->a->xTmp)).xyz;
    rb = glm::vec3((c->p[pointIndex] - c->b->xTmp)).xyz;
  } else {
	  ra = glm::vec3((c->p[pointIndex] - c->a->x)).xyz;
	  rb = glm::vec3((c->p[pointIndex] - c->b->x)).xyz;
  }

  //taking minimum coefficient of friction as discussed in paper's section 7
  float frictionCoeff = 1.0;//(c->a->coeffFriction < c->b->coeffFriction ? 
                        //c->a->coeffFriction : c->b->coeffFriction);

  glm::vec3 vRel = (velA - velB).xyz; 
	float vReln = glm::dot(N, vRel);
  if(!DEBUG) {
    if(vReln > 0) {
      int a, b = 4;
      a=b;
    }
  }

  //in u' = u + Kj, for body "a" and u' = u - Kj for body "b", finding K for a and b
  //K = I/m + (r*)T * (I)-1 * (r*); I is identity matrix, r is ra for "a" and rb for "b" body

  glm::mat3 Ka(0);
  glm::mat3 Kb(0);
  if(!(c->a->isInfiniteMass)) {
    glm::mat3 IinvA;
    if((epsilon < 0.0001) && false){ //disabling this one by 'false'
      IinvA = c->a->getCurrentInertiaInv(true);
    } else {
      IinvA = c->a->getCurrentInertiaInv(false);
    }
    Ka = glm::mat3(1) * c->a->getMassInv() + 
                 glm::transpose(utils::giveVecStar(ra)) * IinvA * utils::giveVecStar(ra);
  }
  if(!(c->b->isInfiniteMass)) {
    glm::mat3 IinvB;// = c->b->getCurrentInertiaInv();
    if((epsilon < 0.0001) && false){ //disabling this one by 'false'
      IinvB = c->b->getCurrentInertiaInv(true);
    } else {
      IinvB = c->b->getCurrentInertiaInv(false);
    }
    Kb = glm::mat3(1) * c->b->getMassInv() +
                   glm::transpose(utils::giveVecStar(rb)) * IinvB * utils::giveVecStar(rb);
  }

  glm::mat3 Kt = Ka + Kb;//NOTE:: if Kt=glm::mat4(0) then??? do we have to check it?
  
  ///PART I - STATIC FRICTION

  //Value of impulse if only static friction applies and stop the tangential motion of bodies
  glm::mat3 invKt = glm::inverse(Kt);
  glm::vec3 j = invKt * ( -vRel - epsilon * vReln * N); 

  if(DEBUG) {//calculating 'j' from notesd, and comparing the normal of impulse
    float numerator = -(1 + epsilon) * vReln;
    float denominator = glm::dot(N, Kt*N);
    float debugJn = numerator/denominator;//Note this must be positive
    if(debugJn < 0) {
      int i=0;
      i*=4;
    }
    if(glm::dot(j,N)<0){
      int i=0;
      i*=4;

    }

  }

  if(!DEBUG) { //Checking final values of 'a' and 'b' on that point
    //Urel+ = Urel- + Kt*j;
    glm::vec3 fvRel = vRel + Kt * j;
    glm::vec4 velChange = - glm::vec4(j,0)*c->b->getMassInv();
    glm::vec4 linearVel = c->b->v - glm::vec4(j,0)*c->b->getMassInv();
    glm::vec3 omegaVel = c->b->omega - c->b->getCurrentInertiaInv(false) * glm::cross(rb, j) * 180.0f/PI;
    glm::vec4 finalVel = linearVel + glm::vec4(glm::cross(omegaVel*PI/180.0f, rb), 0);
    
    glm::vec4 velChangeA = - glm::vec4(j,0)*c->a->getMassInv();
    glm::vec4 linearVelA = c->a->v + glm::vec4(j,0)*c->a->getMassInv();
    glm::vec3 omegaVelA = c->a->omega + c->a->getCurrentInertiaInv(false) * glm::cross(ra, j) * 180.0f/PI;
    glm::vec4 finalVelA = linearVelA + glm::vec4(glm::cross(omegaVelA*PI/180.0f, ra), 0);

    fvRel *= 1;
  }

  //if |j - (j.N)N| <= mu * (j.N), then calculated friction cone is strong enough to hold the 
  //relative tentantial motion and above calculated j is correct one
  float tengentImpulse = glm::length(j - glm::dot(j,N)*N);
  float frictionForce = frictionCoeff * glm::dot(j,N);
  if(!DEBUG) { //if j and N are opposite direction!!!!
    if(frictionForce < 0) {
      if(!utils::epsilonCheck(frictionForce,0) && epsilon > 0.1) {
        int ib=4;
        ib*=4;
      }
      int ia=3;
      ia*= 4;
    }
  }

  if(!DEBUG) {//if we have z component in j!!
    if(!utils::epsilonCheck(j.z,0)) {
      int exp=3;
      exp*=3;
    }
  }

  if( (glm::length(j - glm::dot(j,N)*N) <= frictionCoeff * glm::dot(j,N))/* || (glm::dot(j,N) < 0)*/ ) {
    return j;
  }

  if(!DEBUG) {//my Idea on Frictonal impulse
    //Below idea not working for 
    //jT = j - j.N*N
    glm::vec3 jT = j - glm::dot(j,N)*N;
    jT = glm::normalize(jT);
    jT = jT*frictionCoeff*abs(glm::dot(j,N));
    return jT+glm::dot(j,N)*N;
  }
  if(!DEBUG) {//My second idea on Fricional impulse
    //to calculate frictional cone, consider only tential component of velocity
    glm::vec3 vRelt = vRel - vReln*N;
    float vReltLen = glm::length(vRelt);
    if( utils::epsilonCheck(vReltLen, 0) ) {
      return j;
    }
    //T as tengential direction of collision
    glm::vec3 T = -vRelt / vReltLen;
    if( (glm::dot(j,T) <= frictionCoeff * glm::dot(j,N))/* || (glm::dot(j,N) < 0)*/ ) {
      return j;
    }
  }

  //PART II - KINETIC FRICTION
  //tengential relative velocity is
  glm::vec3 vRelt = vRel - vReln*N;

  float vReltLen = glm::length(vRelt);
  
  //if relative velocity is zero already, then previous j would be apply
  if( utils::epsilonCheck(vReltLen, 0) ) {
    return j;
  }

  //T as tengential direction of collision
  glm::vec3 T = vRelt / vReltLen;

  //from paper, jn = -(epsilon+1)*Urel,n / ( (N)T * Kt * (N - mu*T) ) 
  float jn = -(epsilon+1)*vReln / ( glm::dot(N, Kt * (N - frictionCoeff * T)));
  float jn2 = -(epsilon+1)*vReln / ( glm::dot(N, Kt * (N)));
  j = jn*N - frictionCoeff* jn * T;


  if(DEBUG) {
    //Urel+ = Urel- + Kt*j;
    glm::vec3 fvRel = vRel + Kt * j;
    float fvRelN = glm::dot(fvRel,N);
    if( (utils::epsilonCheck(epsilon,0)) && (abs(fvRelN) > 0.001)) {
      int a=3;
      a*=3;
    }
    glm::vec4 velChange = - glm::vec4(j,0)*c->b->getMassInv();
    glm::vec4 linearVel = c->b->v - glm::vec4(j,0)*c->b->getMassInv();
    glm::vec3 omegaVel = c->b->omega - c->b->getCurrentInertiaInv(false) * glm::cross(rb, j) * 180.0f/PI;
    glm::vec4 finalVel = linearVel + glm::vec4(glm::cross(omegaVel*PI/180.0f, rb), 0);
    
    glm::vec4 velChangeA = - glm::vec4(j,0)*c->a->getMassInv();
    glm::vec4 linearVelA = c->a->v + glm::vec4(j,0)*c->a->getMassInv();
    glm::vec3 omegaVelA = c->a->omega + c->a->getCurrentInertiaInv(false) * glm::cross(ra, j) * 180.0f/PI;
    glm::vec4 finalVelA = linearVelA + glm::vec4(glm::cross(omegaVelA*PI/180.0f, ra), 0);

    fvRel *= 1;
  }


  return j;
}

void simulation::handleCollision(Contact* c) {
	glm::vec4 velA = utils::velocityAtPoint(c->a, c->p[FIRST_POINT_INDEX], false);
	glm::vec4 velB = utils::velocityAtPoint(c->b, c->p[FIRST_POINT_INDEX], false);
	glm::vec4 n = c->n;
	glm::vec4 ra = c->p[FIRST_POINT_INDEX] - c->a->x;
	glm::vec4 rb = c->p[FIRST_POINT_INDEX] - c->b->x;

	double vRel = glm::dot(n, velA-velB);
	double numerator = -(1 + EPSILON) * vRel;

	//Denominator in four parts
	float term1 = c->a->getMassInv();
	float term2 = c->b->getMassInv();
	//term3 and term4 are related to rotational part, lets in beginning we skip this
	//but need to handle this in friction part

	float j = numerator / (term1 + term2);

	//TODO FOR ALL PROJECT: we can change current mechanism instead of velocity based
	// to momentum based, if desire

	//Apply impulse in +n direction of body A and -n direction in body B
	c->a->v +=  c->a->getMassInv() * (j * n);//glm::vec3(n[0], n[1], n[2])) ;
	c->b->v +=  c->b->getMassInv() * -(j * n);// glm::vec3(n[0], n[1], n[2])) ;

	//Need to apply impulse on omega also for rotational motion
}

void simulation::distroyContacts(std::vector<Contact*> &c) {
  for(int i=0; i<c.size(); i++) {
    delete c[i];
  }
  c.clear();
}

void simulation::createDAGGraph(std::set<rigidBody *> &nodes, std::vector< std::pair<rigidBody*, rigidBody*> > &graphEdges, float dt, std::vector<Contact*> &refContact) {
  //loop through each rigid body
  for(int i=0; i<bodies.size(); i++) {  
    //if current rigidBody has inifinite mass, continue it
    if(bodies[i]->getMassInv()==0) continue;

    //update position, (OPTIONALY)rotation of current rigid body
    bodies[i]->xTmp = bodies[i]->x + bodies[i]->v * dt;
    float omegaAbs = glm::dot(bodies[i]->omega, bodies[i]->omega);//glm::length(bodies[i]->omega); 
    if( (omegaAbs > 0.01)) { //by 'false' we disabling rotating	   
      bodies[i]->RTmp = utils::giveRodrignesRot(glm::length(bodies[i]->omega * (float)dt), bodies[i]->omega/glm::length(bodies[i]->omega)) * bodies[i]->R; 
    } else {
      bodies[i]->RTmp = bodies[i]->R;
    }
    //bodies[i]->RTmp = bodies[i]->R;

    //loop thourgh each rigid body except itself
    for(int j=0; j<bodies.size(); j++) {
      if(i==j) break;//NOTE::::: THIS IS BUG, BUT IF ANY OBJECT INTERSECT, IT CAN CREATE LOOP IN DAT, WHICH IN RESEALT HAS INFINITE LOOP AT TOPOLOGICAL SORTING
      //fill temp values with its current values
      bodies[j]->xTmp = bodies[j]->x;
      bodies[j]->RTmp = bodies[j]->R;

      //check for collision with current rigidBody and fill contact
      //NOTE: currently we are handeling only one contact per rigid body
      std::vector<Contact*> c;
      bool result = collisionDetection::detectCollision(bodies[i], bodies[j], c, false, true);

      //TODO:= Just like in all Cubes collision detection, fill up the contact in other methods instead of being manual like below
      if(result) {  
        c.clear();
        result = collisionDetection::detectCollision(bodies[i], bodies[j], c, false, false);
      }
      if(result) {
        refContact.push_back(c[0]);
      }
      
      //if found collision && collision is non separating
      if ( c.size() != 0 /*&& !collisionDetection::seperatingContact(c[0])*/ ) {
        //1. set contacts for each rigid Body
        //NOTE:: There Never be case (as my current thinking) that this contact is already inside both rigid bodies
        bodies[i]->contacts.push_back(c[0]);
        bodies[j]->contacts.push_back(c[0]);

        //2. set edge from 2nd level rigidBody to 1st level rigidBody
        graphEdges.push_back(std::make_pair(bodies[j],bodies[i]));

        //3. add them in our set of edges
        nodes.insert(bodies[i]);
        nodes.insert(bodies[j]);
      } else {
        if(DEBUG) {
          int i=0;
          i *= 9;
        }

      }

    }
    bodies[i]->xTmp = bodies[i]->x;
    bodies[i]->RTmp = bodies[i]->R;
  }
}

void simulation::createDAGGraph2(std::set<rigidBody *> &nodes, std::vector< std::pair<rigidBody*, rigidBody*> > &graphEdges, float dt, std::vector<Contact*> &refContact) {
  //**//(A)We want only one edge between 2 CONVEX OBJECTS. So we do test between 2 objects only once
  //**//(B)We not only check intersection of objects, but we check velocity w.e.t Normal, as well to determine edge direction.
  //****//We consider displace object as object 1. And Normal always pointing from Object 1 to 2.
  //****//if dot(object1.v, normal) positive then edge from 2 to 1. Else edge from 1 to 2.

  //Cache of tmp states so that no need to calculate this every time.
  std::vector<tmpStateCache> tmpStates;
  tmpStates.resize(bodies.size());
  std::vector<bool> isInitialized(bodies.size(), false);

  //loop through all bodies
  for(int i=0; i<bodies.size(); i++) {

    //calculate xTmp and RTmp for current body and save it in cache variable
    if(!isInitialized[i]) {
      utils::fillTmpStates(tmpStates, bodies[i],i,dt);
      isInitialized[i] = true;
    }

    //loop from 1+ current body to remain bodies
    for(int j=i+1; j<bodies.size(); j++) {
        
      //fill xTmp and RTmp for i and j object and check of intersection
      bodies[i]->xTmp = tmpStates[i].xTmp;
      bodies[i]->RTmp = tmpStates[i].RTmp;
      bodies[j]->xTmp = bodies[j]->x;
      bodies[j]->RTmp = bodies[j]->R;


      //NOTE: currently we are handeling only one contact per rigid body
      std::vector<Contact*> c;
      bool result = collisionDetection::detectCollision(bodies[i], bodies[j], c, false, true);

      if(result) {  
        //(as in (B)) get normal pointing from object 1 to 2
        glm::vec4 normal = (bodies[i]==c[0]->b ? c[0]->n : -(c[0]->n));

        //(as in (B)) check wheter object 1 is moving towords or away from 2 via it's velocity and normal value
        if(glm::dot(normal, utils::velocityAtPoint(bodies[i], c[0]->p[0], true)) > 0) {
        
          //if towards then make edge from 2 to 1
          graphEdges.push_back(std::make_pair(bodies[j],bodies[i]));
        } else {
          //else make edge from 1 to 2
          graphEdges.push_back(std::make_pair(bodies[i],bodies[j]));
        }

        refContact.push_back(c[0]);
        bodies[i]->contacts.push_back(c[0]);
        bodies[j]->contacts.push_back(c[0]);
        nodes.insert(bodies[i]);
        nodes.insert(bodies[j]);

      } else {//Else //if not intersect
        //(if not already calculated)calculate xTmp and RTmp for 2nd body and save it in cache variable
        bodies[i]->xTmp = bodies[i]->x;
        bodies[i]->RTmp = bodies[i]->R;
        if(!isInitialized[j]) {
          utils::fillTmpStates(tmpStates, bodies[j] ,j ,dt);
          isInitialized[j] = true;
        }
        bodies[j]->xTmp = tmpStates[j].xTmp;
        bodies[j]->RTmp = tmpStates[j].RTmp;

        //check of intersection
        //NOTE: currently we are handeling only one contact per rigid body
        std::vector<Contact*> c;
        bool result = collisionDetection::detectCollision(bodies[i], bodies[j], c, false, true);
        
        if(result) {
        
          //(as in (B)) get normal pointing from object 2 to 1
          glm::vec4 normal = (bodies[j]==c[0]->b ? c[0]->n : -(c[0]->n));

          //(as in (B)) check wheter object 2 is moving towords or away from 1 via it's velocity and normal value
          if(glm::dot(normal, utils::velocityAtPoint(bodies[j], c[0]->p[0], true)) > 0) {
            //if towards then make edge from 1 to 2
            graphEdges.push_back(std::make_pair(bodies[i],bodies[j]));
          } else {
            //else make edge from 2 to 1
            graphEdges.push_back(std::make_pair(bodies[j],bodies[i]));
          }
          refContact.push_back(c[0]);
          bodies[i]->contacts.push_back(c[0]);
          bodies[j]->contacts.push_back(c[0]);
          nodes.insert(bodies[i]);
          nodes.insert(bodies[j]);

        }
      }
    }
  }
}


void simulation::swapTwoBodiesRandomly() {
  //Finding first random no. with simple method
  float randomNo = utils::getRandomNo(0, bodies.size()-0.01);//get 0 to N-1
  int pos1 = int(randomNo);

  //Finding second random no. by randomly getting obstacle from last no.
  //>Technique is, 0 and N obstacle give same no, so we need 1 to N-1 as
  //>Random obstacle then (pos1 + RandObstacle)%N
  randomNo = utils::getRandomNo(1, bodies.size()-0.01);//get 1 to N-1
  int pos2 = (pos1 + (int)(randomNo)) % bodies.size();

  //Swap bodies
  rigidBody *tmp = bodies[pos1];
  bodies[pos1] = bodies[pos2];
  bodies[pos2] = tmp;

}


glm::vec3 simulation::myHandleCollisionWithFriction(Contact* c, float epsilon, int pointIndex, bool isUseTmp) {

  //**//(A)Calculating impulse based on "notesd2" formulae (8-18)
  //**//(B)We then check the frictional cone, wheter friction due to normal imuplse is enough to make object stop?
  //****//Yes then, calculate frictional Impulse which is < "frictionCoeff*jN"
  //****//No then, frictional impulse is "frictionCoeff*jN" as kinetic friction

  ////Calculating Normal Impulse first without considering friction
  glm::vec4 velA = utils::velocityAtPoint(c->a, c->p[pointIndex], isUseTmp);
	glm::vec4 velB = utils::velocityAtPoint(c->b, c->p[pointIndex], isUseTmp);
	glm::vec3 N = c->n.xyz;
  //ra and rb are just vectors so 4th value always zero
  glm::vec3 ra;
  glm::vec3 rb;
  if(isUseTmp) {
    ra = glm::vec3((c->p[pointIndex] - c->a->xTmp)).xyz;
    rb = glm::vec3((c->p[pointIndex] - c->b->xTmp)).xyz;
  } else {
	  ra = glm::vec3((c->p[pointIndex] - c->a->x)).xyz;
	  rb = glm::vec3((c->p[pointIndex] - c->b->x)).xyz;
  }

  //taking minimum coefficient of friction as discussed in paper's section 7
  float frictionCoeff = 1.0;//(c->a->coeffFriction < c->b->coeffFriction ? 
                        //c->a->coeffFriction : c->b->coeffFriction);

  glm::vec3 vRel = (velA - velB).xyz; 
	float vRelN = glm::dot(N, vRel);

  //in u' = u + Kj, for body "a" and u' = u - Kj for body "b", finding K for a and b
  //K = I/m + (r*)T * (I)-1 * (r*); I is identity matrix, r is ra for "a" and rb for "b" body

  glm::mat3 Ka(0);
  glm::mat3 Kb(0);
  if(!(c->a->isInfiniteMass)) {
    glm::mat3 IinvA = c->a->getCurrentInertiaInv(isUseTmp);
    Ka = glm::mat3(1) * c->a->getMassInv() + 
                 glm::transpose(utils::giveVecStar(ra)) * IinvA * utils::giveVecStar(ra);
  }
  if(!(c->b->isInfiniteMass)) {
    glm::mat3 IinvB;// = c->b->getCurrentInertiaInv();
    IinvB = c->b->getCurrentInertiaInv(isUseTmp);
    Kb = glm::mat3(1) * c->b->getMassInv() +
                   glm::transpose(utils::giveVecStar(rb)) * IinvB * utils::giveVecStar(rb);
  }

  glm::mat3 Kt = Ka + Kb;//NOTE:: if Kt=glm::mat4(0) then??? do we have to check it?
  
  ///PART I - STATIC FRICTION

  float numerator = -(1 + epsilon) * vRelN;
  float denominator = glm::dot(N, Kt*N);
  float jN = numerator/denominator;//Note this must be positive

  if(DEBUG) {
    if(jN<0) {
      int i=0;
      i*=3;
    }
  }

  ////(as in (B))calculating tengential impulse
  ////calculate required impulse in tengential direction to stop motion
  //getting tengential velocity direction
  glm::vec3 vRelT = vRel - vRelN;
  float vRelTLen = glm::dot(vRelT, vRelT);
  if(utils::epsilonCheck(vRelTLen, 0)) {
    return jN*N;
  }
  vRelTLen = glm::sqrt(vRelTLen);
  //::NOTE:: Below is in direction of velocity, N is opposite of velocity
  glm::vec3 T = vRelT/vRelTLen;

  //calculate jT which can stop tengential velocity, similar to jN, just now we have T, instead of N and u'=0
  denominator = glm::dot(T, Kt*T);
  float jT = vRelTLen / denominator;

  if(DEBUG) {
    if(jT<0) {
      int i=0;
      i*=3;
    }
  }

  //check whether jT is less then frictional cone (i.e. u*jN) then its tengential component of j, and return j
  if(jT < (frictionCoeff*jN)) {
    glm::vec3 j = -jT*T + jN*N;
    return j;
  }


  ///PART II - KINETIC FRICTION
  jN = -(epsilon+1)*vRelN / ( glm::dot(N, Kt * (N - frictionCoeff * T)));

  //reaching here means, we need to apply kinetic impulse which is u*jN, in tengential direction
  glm::vec3 j = -frictionCoeff*jN*T + jN*N;
  return j;

}


