#include "cube.h"
#include <iostream>
#include <glm\glm.hpp>
#include "utils.h"
#include "textureUtils.h"

#define BUFFER_OFFSET(val) (void*)val

const int totalIdx= 36;	
const int posCount = 24;

void initializePos (glm::vec4 (&pos)[posCount]);
void initializeNor (glm::vec4 (&nor)[posCount]);
void initializeIdx (int (&idx)[totalIdx]);
void initializeTextureUV(glm::vec2 (&texUV)[posCount]);

using namespace glm;

cube::cube(double inMass = 1, double inCoeffFrictoin) :rigidBody(inMass, inCoeffFrictoin) {
	x = glm::vec4(0, 0, 0, 1); //initial position
	v = glm::vec4(0, 0, 0, 0); //initial velocity

	_scale = glm::vec3(1, 1, 1);

	//initial inertia of body
	Ibody = (float)(inMass / 12) * 
			glm::mat3(2*2 + 2*2, 0, 0,
					  0, 2*2 + 2*2, 0,
					  0, 0, 2*2 + 2*2);
	Ibodyinv = glm::inverse(Ibody);

	bodyType = rigidBody::CUBE;

  int sideLen = 1;
  //Top face
  positions.push_back(glm::vec4( sideLen, sideLen,  sideLen, 1) );
  positions.push_back(glm::vec4( sideLen, sideLen, -sideLen, 1) );
  positions.push_back(glm::vec4(-sideLen, sideLen, -sideLen, 1) );
  positions.push_back(glm::vec4(-sideLen, sideLen,  sideLen, 1) );
  //Bottom face
  positions.push_back(glm::vec4( sideLen, -sideLen,  sideLen, 1) );
  positions.push_back(glm::vec4( sideLen, -sideLen, -sideLen, 1) );
  positions.push_back(glm::vec4(-sideLen, -sideLen, -sideLen, 1) );
  positions.push_back(glm::vec4(-sideLen, -sideLen,  sideLen, 1) );

  updatedPos = positions;

  ////Storing faces for getting edge of face
  //Storing face at +X Axis
  //NOTE::We are not convern here of Clock wise or other way storage
  //BIG NOTE:::: WE ARE TAKING ADVANTAGE OF BELOW ORDER ON + & - AXIS IN "getCurrentAxisParallelEdge" FUNCTION. SO IF CHANGING THIS, TAKE CARE OF THAT FUNCTION
  for(int i=0; i<2; i++) {
    face fX, fY, fZ;
    int pOrNLen = (i ? -1 : 1);//Positive or negative
    fX.push_back(glm::vec4( pOrNLen,  sideLen,  sideLen, 1));
    fX.push_back(glm::vec4( pOrNLen, -sideLen,  sideLen, 1));
    fX.push_back(glm::vec4( pOrNLen, -sideLen, -sideLen, 1));
    fX.push_back(glm::vec4( pOrNLen,  sideLen, -sideLen, 1));

    fY.push_back(glm::vec4( sideLen,  pOrNLen,  sideLen, 1));
    fY.push_back(glm::vec4( sideLen,  pOrNLen, -sideLen, 1));
    fY.push_back(glm::vec4(-sideLen,  pOrNLen, -sideLen, 1));
    fY.push_back(glm::vec4(-sideLen,  pOrNLen,  sideLen, 1));

    fZ.push_back(glm::vec4( sideLen,  sideLen,  pOrNLen, 1));
    fZ.push_back(glm::vec4(-sideLen,  sideLen,  pOrNLen, 1));
    fZ.push_back(glm::vec4(-sideLen, -sideLen,  pOrNLen, 1));
    fZ.push_back(glm::vec4( sideLen, -sideLen,  pOrNLen, 1));

    faces.push_back(fX);
    faces.push_back(fY);
    faces.push_back(fZ);   
  }
}

void cube::create()
{
	//initialize count
	count = totalIdx;

	///create array to store index, position and normal
	glm::vec4 pos[posCount];
	glm::vec4 nor[posCount];
	int idx[totalIdx];
  glm::vec2 texUV[posCount];

	///initialize above arrays by calling their routines
	initializePos(pos);
	initializeNor(nor);
	initializeIdx(idx);
  initializeTextureUV(texUV);
	
	//send position
	glGenBuffers(1, &bufPos);
	glBindBuffer(GL_ARRAY_BUFFER, bufPos);
	glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec4)*posCount, pos, GL_STATIC_DRAW);

	//send normal
	glGenBuffers(1, &bufNor);
	glBindBuffer(GL_ARRAY_BUFFER, bufNor);
	glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec4)*posCount, nor, GL_STATIC_DRAW);

  //set UV
  glGenBuffers(1, &bufUV);
  glBindBuffer(GL_ARRAY_BUFFER, bufUV);
  glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec2)*posCount, texUV, GL_STATIC_DRAW);

	//send index
	glGenBuffers(1, &bufIdx);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, bufIdx);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(int)*totalIdx, idx, GL_STATIC_DRAW);
}

cube::~cube(void)
{
}

GLuint cube::idxCount() {
	return count;
}

GLboolean cube::bindIdx() {
  if(glIsBuffer(bufIdx)) {
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, bufIdx);
    return true;
  }
  return false;
}

GLboolean cube::bindPos() {
  if(glIsBuffer(bufPos)) {
    glBindBuffer(GL_ARRAY_BUFFER, bufPos);
    return true;
  }
  return false;
}

GLboolean cube::bindNor() {
  if(glIsBuffer(bufNor)) {
    glBindBuffer(GL_ARRAY_BUFFER, bufNor);
    return true;
  }
  return false;
}

GLboolean cube::bindUV() {
  if(glIsBuffer(bufUV)) {
    glBindBuffer(GL_ARRAY_BUFFER, bufUV);
    return true;
  }
  return false;
}

GLboolean cube::bindTexture() {
  if(glIsTexture(textureID)) {
    glBindTexture(GL_TEXTURE_2D, textureID);
    return true;
  }
  return false;
}

//scaling functions
void cube::setScale(float scaleX, float scaleY, float scaleZ) {
	_scale[0] = scaleX;
	_scale[1] = scaleY;
	_scale[2] = scaleZ;

  if(getMassInv()!=0) {
    //changing inertia of body
    float newX = 2*scaleX, newY = 2*scaleY, newZ = 2*scaleZ;
    Ibody = (float)(getMass() / 12) * 
			  glm::mat3(newY*newY + newZ*newZ, 0                    , 0                     ,    //y2+z2 0     0
					        0                    , newX*newX + newZ*newZ, 0                     ,        //0     x2+z2 0
					        0                    , 0                    , newX*newX + newY*newY);       //0     0     x2+y2
	  Ibodyinv = glm::inverse(Ibody);
  }
}

void cube::setTexture(const char* fileName) {
  int width, height;
  GLubyte *image;
   textureUtils::loadBMPTexture(fileName, (unsigned char **)&image, &width, &height);
  CHECK_OGL_ERROR
  //Generating texture data in GPU
  glGenTextures(1, &textureID);
  glBindTexture(GL_TEXTURE_2D, textureID);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_BGR, GL_UNSIGNED_BYTE, image);
  delete []image;

  //Setting wrapping/filtering option for currently bounded texture
  //Now set how you want to use this across U and V direction, because glBindTexture first time set default state
  //1. Repetaion of image across U and V direction if U/V value will > 1 or < 0
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_MIRRORED_REPEAT);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_MIRRORED_REPEAT);

  //2. Set filter for Minification and Magnification
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);//gl_linear, take average of near pixel to avoid anialiasing
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

  CHECK_OGL_ERROR
}

glm::mat4 cube::getScaleMatix() {
	return glm::mat4( _scale[0]	, 0			, 0			, 0,
					  0			,_scale[1]	, 0			, 0,
					  0			, 0			, _scale[2]	, 0,
					  0			, 0			, 0			, 1 );
}

glm::mat4 cube::getModelMatrix(bool isUseTmp) {
  if(isUseTmp) {
  	return glm::mat4( 1		, 0		, 0		, 0,
					  0		, 1		, 0		, 0,
					  0		, 0		, 1		, 0,
            xTmp[0]	, xTmp[1]	, xTmp[2]	, 1 ) * utils::mat3ToMat4(RTmp) * getScaleMatix();
  } else {
	  return glm::mat4( 1		, 0		, 0		, 0,
					    0		, 1		, 0		, 0,
					    0		, 0		, 1		, 0,
					    x[0]	, x[1]	, x[2]	, 1 ) * utils::mat3ToMat4(R) * getScaleMatix();
  }
}


const std::vector<glm::vec4> &cube::getCurrentPositoins(bool isUseTmp) {
  glm::mat4 transform;
  transform = getModelMatrix(isUseTmp);

  for(int i=0; i<8; i++) {
    updatedPos[i] = transform * positions[i];
  }
  return updatedPos;
}

glm::vec3 cube::getHalfDimension() {
  return _scale;
}

std::vector<glm::vec4> cube::getCurrentFace(int axisId, bool isPositiveSide, bool isUseTmp) {
  face f;
  const int allPositiveAxises = 3;

  //get origin position of requested face
  if(isPositiveSide) {
    f = faces[axisId];
  } else {
    f = faces[allPositiveAxises + axisId];
  }

  //get current position of requested face and return it
  glm::mat4 transform = getModelMatrix(isUseTmp);
  for(int i=0; i<4; i++) {
    f[i] = transform * f[i];
  }

  return f;
}

std::vector<edge> cube::getCurrentAxisParallelEdge(int axisId, bool isUseTmp) {
  std::vector<edge> outEdges;
  const int totalAxis = 3;
  glm::mat4 transform = getModelMatrix(isUseTmp);
  
  //loop for 4 posible parallel edges
  for(int i=0; i<4; i++) {
    //get current transform values of those edges
    edge e;
    e.first = transform*faces[axisId][i];
    e.second = transform*faces[totalAxis + axisId][i];
    outEdges.push_back(e);
  }
  return outEdges;
}


void initializePos (glm::vec4 (&pos)[posCount]) {
	//Front Face z=1.0
	pos[0] = vec4(1.0, 1.0, 1.0, 1.0); //t-r = top - right
	pos[1] = vec4(-1.0, 1.0, 1.0, 1.0); //t-l
	pos[2] = vec4(-1.0, -1.0, 1.0, 1.0); //l-b = left-bottom
	pos[3] = vec4(1.0, -1.0, 1.0, 1.0); //r-b 

	//Right Face x = 1.0
	pos[4] = vec4( 1.0 , 1.0 , -1.0 , 1.0); //ba-t = back-top
	pos[5] = vec4( 1.0 , 1.0 , 1.0 , 1.0); //f-t
	pos[6] = vec4( 1.0 , -1.0 , 1.0 , 1.0); //bo-f = bottom - front
	pos[7] = vec4( 1.0 , -1.0 , -1.0 , 1.0); //bo-ba

	//Back Face z = -1.0
	pos[8] = vec4( -1.0 , 1.0 , -1.0 , 1.0); //t-l
	pos[9] = vec4( 1.0 , 1.0 , -1.0 , 1.0); //t-r
	pos[10] = vec4( 1.0 , -1.0 , -1.0 , 1.0); //b-r
	pos[11] = vec4( -1.0 , -1.0 , -1.0 , 1.0); //b-l

	//Left Face  x = -1.0
	pos[12] = vec4( -1.0 , 1.0 , 1.0 , 1.0); //n-t
	pos[13] = vec4( -1.0 , 1.0 , -1.0 , 1.0); //b-t
	pos[14] = vec4( -1.0 , -1.0 , -1.0 , 1.0); //b-bo
	pos[15] = vec4( -1.0 , -1.0 , 1.0 , 1.0); //bo-n

	//Top Face y = 1.0
	pos[16] = vec4( -1.0 , 1.0 , 1.0 , 1.0); //n-l
	pos[17] = vec4( 1.0 , 1.0 , 1.0 , 1.0); //n-r
	pos[18] = vec4( 1.0 , 1.0 , -1.0 , 1.0); //b-r
	pos[19] = vec4( -1.0 , 1.0 , -1.0 , 1.0); //b-l

	//Bottom Face y = -1.0
	pos[20] = vec4( 1.0 , -1.0 , 1.0 , 1.0); //r-n
	pos[21] = vec4( -1.0 , -1.0 , 1.0 , 1.0); //l-n
	pos[22] = vec4( -1.0 , -1.0 , -1.0 , 1.0); //ba-l
	pos[23] = vec4( 1.0 , -1.0 , -1.0 , 1.0); //r-ba

}

void initializeNor (glm::vec4 (&nor)[posCount]) {
	//Front Face
	for(int i=0; i<4; i++)
		nor[i] = vec4(0.0, 0.0, 1.0, 0.0); //+z

	//Right Face
	for(int i=0; i<4; i++)
		nor[4+i] = vec4(1.0, 0.0, 0.0, 0.0);//+x

	//Back Face
	for(int i=0; i<4; i++)
		nor[8+i] = vec4(0.0, 0.0, -1.0, 0.0);//-z

	//Left Face
	for(int i=0; i<4; i++)
		nor[12+i] = vec4(-1.0, 0.0, 0.0, 0.0);//-x

	//Top Face
	for(int i=0; i<4; i++)
		nor[16+i] = vec4(0.0, 1.0, 0.0, 0.0);//+y

	//Bottom Face
	for(int i=0; i<4; i++)
		nor[20+i] = vec4(0.0, -1.0, 0.0, 0.0);//-y

}

void initializeIdx (int (&idx)[totalIdx]) {
	
	for(int i=0; i<6; i++) {
		idx[i*6 + 0] = i*4 + 0;
		idx[i*6 + 1] = i*4 + 1;
		idx[i*6 + 2] = i*4 + 2;
		idx[i*6 + 3] = i*4 + 0;
		idx[i*6 + 4] = i*4 + 2;
		idx[i*6 + 5] = i*4 + 3;
	}
	
}

void initializeTextureUV(glm::vec2 (&texUV)[posCount]) {
	//Front Face z=1.0
	texUV[0] = vec2(0.5, 0.5); //t-r = top - right
	texUV[1] = vec2(0.25, 0.5); //t-l
	texUV[2] = vec2(0.25, 0.25); //l-b = left-bottom
	texUV[3] = vec2(0.5, 0.25); //r-b 

	//Right Face x = 1.0
	texUV[4] = vec2( 0.75, 0.5); //ba-t = back-top
	texUV[5] = vec2( 0.5 , 0.5); //f-t
	texUV[6] = vec2( 0.5 , 0.25); //bo-f = bottom - front
	texUV[7] = vec2( 0.75 , 0.25); //bo-ba

	//Back Face z = -1.0
	texUV[8] = vec2( 1.0, 0.5); //t-l
	texUV[9] = vec2( 0.75, 0.5); //t-r
	texUV[10] = vec2( 0.75, 0.25); //b-r
	texUV[11] = vec2( 1.0, 0.25); //b-l

	//Left Face  x = -1.0
	texUV[12] = vec2( 0.25 , 0.5); //n-t
	texUV[13] = vec2( 0.0 , 0.5); //b-t
	texUV[14] = vec2( 0.0 , 0.25); //b-bo
	texUV[15] = vec2( 0.25 , 0.25); //bo-n

	//Top Face y = 1.0
	texUV[16] = vec2( 0.25 , 0.25); //n-l
	texUV[17] = vec2( 0.5 , 0.5); //n-r
	texUV[18] = vec2( 0.5 , 0.75); //b-r
	texUV[19] = vec2( 0.75 , 0.25); //b-l

	//Bottom Face y = -1.0
	texUV[20] = vec2( 0.5 , 0.25); //r-n
	texUV[21] = vec2( 0.25 , 0.25); //l-n
	texUV[22] = vec2( 0.25 , 0.0); //ba-l
	texUV[23] = vec2( 0.5 , 0.0); //r-ba

}

