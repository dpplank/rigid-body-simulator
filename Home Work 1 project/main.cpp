#include <stdio.h>
#include <conio.h>
#include <iostream>
#include <string>
#include <sstream>
#define GLM_SWIZZLE //It need to define before any GLM inclusion for
					//for swizzle like GLSL ex if vec4 r; then r.xyx; is valid type of things
#include "shader.h"
#include <GL/freeglut.h>
#include <ctime>
#include <iomanip>  //for std::setPrecision

#include "cube.h"
#include "sphere.h"
#include "ground.h"
#include "camera.h"
#include "utils.h"
#include "mouseHandling.h"
#include "simulation.h"
#include "sceneStructure.h"

#define DEBUG 1

using namespace std;

//Contains all mouse handlings
mouseHandling mouseHandle;

//Conatians all simulation and position update code
simulation mainSimulation;

//sceneStructure to handle all scene related things
sceneStructure currentScene;

//cube cubeGround(10000);

camera c;
shader lambertShad;
shader textureShad;
shader lineShad;

glm::mat4 viewProject;

bool showWireFrame;

void init();
void keyPressControl(unsigned char key, int x, int y);
void keyReleaseControl(unsigned char key, int x, int y);
void reshapeWindow(int widht, int height);
void specialKeyPressControl(int key, int x, int y);
void specialKeyReleaseControl(int key, int x, int y);
//time_t oldTime;
int oldTime;
int itrTimes=0;

void idle() {
  itrTimes++;
	//time_t newTime;
	//time(&newTime);
  //static int oldTime = glutGet(GLUT_ELAPSED_TIME);

	int newTime = glutGet(GLUT_ELAPSED_TIME); //time in mili seconds

	//cout<<"clock: "<<(newTime-oldTime)<<endl;
  //getting time step in seconds
	double deltaT = (double)(newTime - oldTime) / 1000.0; 
	oldTime = newTime;

  float debugTime = 1/60.0;
	//calculate dt and call simulate
	//mainSimulation.simulate(deltaT);
  
  //mainSimulation.simulateOld(debugTime);
  mainSimulation.simulateOptimized(debugTime);

  //CHECK_OGL_ERROR
	glutPostRedisplay();
  //CHECK_OGL_ERROR
}

void showFPS() {
  static int fpsStartTime = glutGet(GLUT_ELAPSED_TIME);
  static long totalFrames = 0;
  totalFrames++;

  int newTime = glutGet(GLUT_ELAPSED_TIME); //time in mili seconds
  int dt = newTime - fpsStartTime;
  
  if(dt > 1) {
    //total frames rendered / total time passed
    int currentFPS = 1000 * totalFrames / (float)dt;
    
    stringstream ss;
    ss<<"Hare Krishna : Current FPS is: ";
    //ss<<std::setprecision(3)<< currentFPS;
    ss<<currentFPS;

    ss<<": Lapsed Time: "<< std::setprecision(2)<< dt/1000.0f;

    ss<<": iterations: "<<itrTimes;

    //Also showing camera zoom value
    ss<<": And camera zoom value is: "<<c.zoom;
    glutSetWindowTitle(ss.str().c_str());
  }
}

void render() {
  CHECK_OGL_ERROR;
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  showFPS(); 
	//Loading view matrix into shader
  glm::mat4 viewProj =  c.perspective() * c.view();//c.perspective() * c.view();
	//lambertShad.loadViewProj((viewProj));
  //currentScene.renderScene(lambertShad);
  
  textureShad.loadViewProj(viewProj);
  currentScene.renderScene(textureShad, shader::drawType::TRIANGLE);

	//glutPostRedisplay();
 
  if(showWireFrame) {
    lineShad.loadViewProj(viewProj);
    currentScene.renderScene(lineShad, shader::drawType::LINE);
  }

	glutSwapBuffers();

  CHECK_OGL_ERROR
}

int main(int argc, char **argv) {
	cout<<"Hare Krishna"<<endl;

	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH);
	//Here gl functions not work, because they are not initialized yet
	//glEnable(GL_CULL_FACE);
	//glDepthFunc(GL_LESS);
	glutInitWindowSize(900, 500);
	glutCreateWindow("Hare Krishna");

	GLenum err= glewInit();
	if(err!=GLEW_OK){
		cerr<<"Unable to initialize glew"<<endl;
		system("pause");
		exit(0);
	}
	glEnable(GL_DEPTH_TEST);
  CHECK_OGL_ERROR
	init();
  CHECK_OGL_ERROR

	//Key Handling function setup in glut
	glutDisplayFunc(render);
	glutKeyboardFunc(keyPressControl);
  glutKeyboardUpFunc(keyReleaseControl);
  glutReshapeFunc(reshapeWindow);
  glutSpecialFunc(specialKeyPressControl);
  glutSpecialUpFunc(specialKeyReleaseControl);

	//Mouse handling funciton setup in glut
	glutMouseFunc(&(mouseHandling::mouseClicking));
	glutMotionFunc(&(mouseHandling::mouseDragging));
	
	//Idle function for simulation
	glutIdleFunc(idle);    //FOR TEMP DEBUG TEXTURE

  oldTime = glutGet(GLUT_ELAPSED_TIME);
	glutMainLoop();

	system("pause");
  return 0;
}

void init() {
	//set clear color
	glClearColor(0,0,0.5,1);

	//Set appropriate camera to mouseHandler and mainSimulation
	mouseHandle.setCamera(&c);
  mouseHandle.setScene(&currentScene);

  mainSimulation.setCamera(&c);

	//create VAO and bind that
	GLuint vao;
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	/** setting initial camera parameters **/
	c.zoom	= 10;
	c.phi	= 15;
	c.theta = 0;

  /** Setting Texture Initialization setting here **/
  //Initializing texturing
  glEnable(GL_TEXTURE_2D); //will enable Texture Unit 0 by default
  CHECK_OGL_ERROR

	/** create your objects here **/
   currentScene.createScene();
  currentScene.copyAllbodies(mainSimulation.bodies);
  currentScene.setSimulation(&mainSimulation);

	/** create shaders here and set initializing parameter **/

	lambertShad.create("vertex shader.txt", "fragment shader.txt");

  CHECK_OGL_ERROR
  textureShad.create("vTexShader.glsl", "fTexShader.glsl");
  CHECK_OGL_ERROR

  lineShad.create("vLineShader.glsl", "fLineShader.glsl");
  CHECK_OGL_ERROR
	//Initializing view matrix into shader
	//lambertShad.loadModelMat(glm::mat4(1));
	//lambertShad.loadViewProj(c.perspective() * c.view());
	//lambertShad.loadUnifColor(glm::vec4(1,0,1,1));
  //textureShad.loadViewProj(c.perspective() * c.view());
  
  if(DEBUG) {
    //This gives range of supported line widht, which is 1 to 10 now
    float range[2] ;
    glGetFloatv(GL_ALIASED_LINE_WIDTH_RANGE, range);
    range [1]*= 1;
  }
  glLineWidth(3);

  showWireFrame = true;
}

void keyPressControl(unsigned char key, int x, int y) {
	if(key == 27)
		exit(0);
  
  static bool paused = false;
  if(key == 'p' || key == 'P') {  
    if(paused) {
      oldTime = glutGet(GLUT_ELAPSED_TIME);
      glutIdleFunc(idle);
      paused = false;
    } else {
      glutIdleFunc(nullptr);
      paused = true;
    }
  }

  //For enable/disable shooting ball on mouse click,
  if(key == ' ') {
    if(mouseHandle.getCtrlClick()) {
      mouseHandle.setCtrlClick(false);
    } else {
      mouseHandle.setCtrlClick(true);
    }
  }

  //for stepping per frame
  //NOTE: this functoin works only for 1/60 as dt time for simulation, because we are
  //      not updating "oldTime" function
  if(key == 's') {
    glutIdleFunc(nullptr);
    idle();
    paused=true;
  }

  //Key for enabling and disabling Edge mode rendering 
  if(key == 'e') {
    if(showWireFrame) {
      showWireFrame = false;
    } else {
      showWireFrame = true;
    }
  }
}

void keyReleaseControl(unsigned char key, int x, int y) {

}

bool isCtrlPressed=false;

void specialKeyPressControl(int key, int x, int y) {
  if(key == GLUT_KEY_RIGHT) {
    //c.ref.x += 1.0;
    glm::vec3 toMove = utils::getRotation(glm::vec3(1,0,0), glm::vec3(0,1,0), c.theta);
    c.ref += glm::vec4(toMove,0);
    glutPostRedisplay();
  }

  if(key == GLUT_KEY_LEFT) {
    glm::vec3 toMove = utils::getRotation(glm::vec3(1,0,0), glm::vec3(0,1,0), c.theta);
    c.ref -= glm::vec4(toMove,0);
    glutPostRedisplay();
  }

  if(key == GLUT_KEY_UP) {

    if(!isCtrlPressed) {
      //move c.ref in X-Z plane,forwardly along direction camera is viewing
      glm::vec3 toMove = utils::getRotation(glm::vec3(0,0,-1), glm::vec3(0,1,0), c.theta);
      //c.ref.z -= 1.0;
      c.ref += glm::vec4(toMove,0);
      glutPostRedisplay();
    } else {
      //For upside and downside
      glm::vec3 toMove(0,1,0);
      c.ref += glm::vec4(toMove,0);
      glutPostRedisplay();
    }
  }
    
  if(key == GLUT_KEY_DOWN) {
    if(!isCtrlPressed) {
      //move c.ref in X-Z plane, backwardly along direction camera is viewing
      glm::vec3 toMove = utils::getRotation(glm::vec3(0,0,-1), glm::vec3(0,1,0), c.theta);
      c.ref -= glm::vec4(toMove,0);
      glutPostRedisplay();
    } else {
      //For upside and downside
      glm::vec3 toMove(0,1,0);
      c.ref -= glm::vec4(toMove,0);
      c.ref.y = std::max(c.ref.y, 0.0f);
      glutPostRedisplay();
    }
  }

  if(key == GLUT_KEY_CTRL_L || key == GLUT_KEY_CTRL_R) {
    isCtrlPressed=true;
  }
}

void specialKeyReleaseControl(int key, int x, int y) {
  if(key == GLUT_KEY_CTRL_L || key == GLUT_KEY_CTRL_R) {
    isCtrlPressed=false;
  }
}

void reshapeWindow(int width, int height){
  c.setWidthHeight(width, height);
  glViewport(0,0,width, height);
  //glutReshapeWindow(width, height);
}