#ifndef CAMERA_H
#define CAMERA_H

#include <glm/glm.hpp>


class camera
{
public:
    // for view matrix
    //phi :- angle from x-z plane
    //theta :- angle from y-z plane
    float theta, phi;
    float zoom;
    glm::vec4 ref; // view reference for paning of camera

    // for projection matrix
    float fovy;
    float width, height;
    float nearP, farP;

    camera();
    camera(float inWidth, float inHeight );
    ~camera();

    ///Setter Functions///
    void setWidthHeight(float inWidht, float inHeight);

    ///Getter Functions///
    glm::mat4 perspective();
    glm::mat4 view();
    glm::mat4 orthogonal();
    
    glm::vec4 getPosition(); // return position of camera
    glm::vec4 getRight();
    glm::vec4 getUp();
    glm::vec4 getLookAtDir();

};

#endif // CAMERA_H
