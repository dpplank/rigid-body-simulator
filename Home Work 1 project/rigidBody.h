﻿#pragma once
#include <glm/glm.hpp>
#include <vector>
#include "contact.h"

class rigidBody
{
public:
  enum BodyTypes {UNKNOWN, GROUND, SPHERE, CUBE};

  //inMass<0, shows infinite mass
	rigidBody(double inMass, double inCoeffFriction);
  //IMP:  virtual distructor can enable "delete" operation on 'rigidBody' type pointer
	virtual ~rigidBody(void);
	
  ///Setter Functions///
  void setColor(glm::vec4 inColor);
  void setRotation(glm::vec3 axis, float angleInDegree);
  void addToQueueMomentum(glm::vec3 inJ, glm::vec3 inR, bool isUseTmp);
  void setOmegaRad(glm::vec3 inOmega); //Set Omega in radians

  ///Getter Functions///
  glm::vec4 getColor();

	float getMass();
	float getMassInv();
	BodyTypes getType();

	glm::mat3 getRotationMatrix(bool isUseTmp);
	glm::mat4 giveCurrentPosMatrix(bool isUseTmp);
  virtual glm::mat4 getModelMatrix(bool isUseTmp)=0;
  glm::mat3 getCurrentInertia(bool isUseTmp);
  glm::mat3 getCurrentInertiaInv(bool isUseTmp);

  //getOmega in radians
  glm::vec3 getOmegaRad(); //inRadian


	//Apply force and calculate corrosponding torque by that
	//force at "position" in object coordinate
	void applyForceTorque(glm::vec3 inForce, glm::vec3 position);
  void applyQueuedMomentum();
  //Apply Impulse, to linear as well as rotational motion
  void applyImpulse(glm::vec3 inJ, glm::vec3 position, bool isUseTmp);
  //Apply Impulse to only linear motion. Hint from http://www.rowlhouse.co.uk/jiglib/
  void applyLinearImpulse(glm::vec3 inJ);

  //Clearing all contacts
  void clearContacts();

public:
	
	//type of body this represent
	BodyTypes bodyType;

	//constant quantity
	float massInv;		//Mass M
	glm::mat3 Ibody,	//Intertia in body space
			  Ibodyinv;	//Inverse of inertia for calculating primarily angular velocity from angular momentum


	//state variables
	glm::vec4 x;	/*x(t)*/ //Position of ceter of mass in world
	glm::mat3 R;	/*R(t)*/ //Orientation of rigid body
	glm::vec4 P;	/*P(t)*/ //Linear momentum
	glm::vec4 L;	/*L(t)*/ //Angular momentum


  //Tmp state variables
  glm::vec4 xTmp;
  glm::mat3 RTmp;

  glm::vec4 queueVelChange;
  glm::vec3 queueOmegaChange;

	//derived quantities
	glm::mat3 I, Iinv;	/*I−1(t)*/ //Inertia in world space
	glm::vec4 v;	/*v(t)*/ //linear velocity of center of mass
  //SPECIAL NOTE:: "omega" is in (degree/second) instead of (radian/second)
	glm::vec3 omega;	/*w(t)*/ //Angular velocity of rigid body about axis passing through COM

	
	//computed quantities
	glm::vec4 force;	/*F(t)*/	//total force acting on this rigid body
	glm::vec3 torque;	/*T(t)*/	//total torque acting on this rigid body

  //To render this rigid body, its color
  glm::vec4 color;
  
  ///EXTRA: For various algorithmic porpouse such as conact resolution
  std::vector<Contact*> contacts;
  //This variable set where this object lies in level
  int level;
  //below boolean will use to freeze object, for Shock Propogation 
  //during contact resolution
  bool isInfiniteMass;


  //Tmp 

  //we use minimum of coeffFriction of two rigidBody's for collision
  float coeffFriction;

};

