#define GLM_SWIZZLE
#include <numeric>
#include <algorithm>

#include "collisionDetection.h"
#include "utils.h"

#define DEBUG 0
#define EDGE_TEST_ENABLED 1
const float THERSHOLD = 0.01;


collisionDetection::collisionDetection(void) {
}

collisionDetection::~collisionDetection(void) {
}


bool collisionDetection::collidingContact( Contact *c, int pointIndex, bool isUseTmp) {
	glm::vec4 velocityA = utils::velocityAtPoint(c->a, c->p[pointIndex], isUseTmp);
	glm::vec4 velocityB = utils::velocityAtPoint(c->b, c->p[pointIndex], isUseTmp);

	float vRel = glm::dot(c->n, velocityA-velocityB);

	if (vRel > THERSHOLD) { //seperating contact
		return false;
	} if (vRel >-THERSHOLD) { //resting contact
		return false;
	} else {//(vRel < -THERSHOLD) colliding contact
		return true;
	} 
}

bool collisionDetection::seperatingContact( Contact *c, int pointIndex, bool isUseTmp) {
	glm::vec4 velocityA = utils::velocityAtPoint(c->a, c->p[pointIndex], isUseTmp);
	glm::vec4 velocityB = utils::velocityAtPoint(c->b, c->p[pointIndex], isUseTmp);

	float vRel = glm::dot(c->n, velocityA-velocityB);

	if (vRel > 0) { //seperating contact
		return true;
	} if (vRel >-THERSHOLD) { //resting contact
		return false;
	} else {//(vRel < -THERSHOLD) colliding contact
		return false;
	} 
}

bool collisionDetection::discoverCollision(const std::vector<rigidBody*> &bodies, std::vector<Contact*> &contacts, bool isForCollision, bool isUseTmp) {
	bool isCollision = false;
	for(int i=0; i<bodies.size(); i++) {
		for(int j=i+1; j<bodies.size(); j++) {
			bool result = collisionDetection::detectCollision(bodies[i], bodies[j], contacts, isForCollision, isUseTmp);
			if(result) {
        isCollision = true;
			}
		}
	}
	return isCollision;
}

bool collisionDetection::detectCollision(rigidBody *r1, rigidBody *r2, std::vector<Contact*>& contacts,bool isForCollision, bool isUseTmp) {
	//check what r1 and r2 represents and call appropriate routines
	if(r1->getType() == rigidBody::UNKNOWN || r2->getType() == rigidBody::UNKNOWN) {
		return false;
	}

  switch(r1->getType() ) {
  //FIRST IS SPHERE
  case rigidBody::SPHERE: {
    switch( r2->getType() ) {
    case rigidBody::SPHERE: {
      return sphereSphere((sphere*)r1, (sphere*)r2, contacts, isUseTmp);
      break;
    }
    case rigidBody::GROUND: {
      return sphereGround((sphere*)r1, (ground*)r2, contacts, true, isUseTmp);
      break;
    }
    case rigidBody::CUBE: {
      return cubeSphere((cube*)r2, (sphere*)r1, contacts, isUseTmp);
      break;
    }
    }
    break;
  }
  //SECOND IS GROUND
  case rigidBody::GROUND: {
    switch( r2->getType() ) {
    case rigidBody::SPHERE: {
      return sphereGround((sphere*)r2, (ground*)r1, contacts, false, isUseTmp);
      break;
    }
    case rigidBody::CUBE: {
      return cubeGround((cube*)r2, (ground*)r1, contacts, isForCollision, isUseTmp);
      break;
    }
    }
    break;
  }
  //THIRD IS CUBE
  case rigidBody::CUBE: {
    switch( r2->getType() ) {
    case rigidBody::GROUND : {
      return cubeGround((cube*)r1, (ground*)r2, contacts, isForCollision, isUseTmp);
      break;
    }
    case rigidBody::SPHERE: {
      return cubeSphere((cube*)r1, (sphere*)r2, contacts, isUseTmp);
      break;
    }
    case rigidBody::CUBE: {
      return cubeCube((cube*)r1, (cube*)r2, contacts, isForCollision, isUseTmp);
      break;
    }
    }
    break;
  }
  }

  return false;
	//if(r1->getType() == rigidBody::SPHERE) {
	//	if(r2->getType() == rigidBody::SPHERE) {
	//		return sphereSphere((sphere*)r1, (sphere*)r2, normal, point);
	//	} else {
	//		return sphereGround((sphere*)r1, (ground*)r2, normal, point, true);
	//	}
 // } else if(r2->getType() == rigidBody::SPHERE){
	//	//r2 must be sphere then
	//	bool result =  sphereGround((sphere*)r2, (ground*)r1, normal, point, false); 
 //   return result;
 //   
	//} else {
 //   //r1 and r2 representing ground so return false
 //   return false;
 // }

}

bool collisionDetection::sphereSphere(sphere *s1, sphere *s2, std::vector<Contact*> &contacts, bool isUseTmp) {
  glm::vec4 centerVector;
  if(isUseTmp) {
    centerVector = s1->xTmp - s2->xTmp;
  } else {
    centerVector = s1->x - s2->x;
  }
	float centerDist = glm::dot(centerVector,centerVector) ;
	float totalRadius = s1->getRadius() + s2->getRadius();

	if(centerDist > (totalRadius*totalRadius)) {
		return false;
	} else {
    Contact *cont = new Contact;
    cont->a = s1;
    cont->b = s2;
		glm::vec4 unitCenterVector = glm::normalize(centerVector);
    if(isUseTmp) {
      cont->p.push_back(s2->xTmp + unitCenterVector*s2->getRadius());
    } else {
      cont->p.push_back(s2->x + unitCenterVector*s2->getRadius());
    }

		cont->n = unitCenterVector;
    contacts.push_back(cont);
		return true;
	}

}

bool collisionDetection::sphereGround1(sphere *s, ground *g, std::vector<Contact*> &contacts, bool inOrder, bool isUseTmp) {
	//hare Krishna Hare Krihsna KrishnaKrishan Hare Hare
	//hare Rama hare Rama RamaRama Hare Hare
	///BASE FORMULAE for working is, 
	/// if L = <N,D> represent plane, here N is normal to plane and D is any point lie on plane
	/// And P represent center of sphere then
	/// L.P or N.P + D > r if sphere is not colliding, else it is colliding
	/// where D = -N.Q and Q is any point lies in plane

	glm::vec3 N = g->getNormal();
	glm::vec4 Q = g->getPointOnPlane();
	glm::vec4 N4 = glm::vec4(N,0.0);
	float D = -1 * glm::dot(N4,Q);
	
	glm::vec4 P = s->getCenter(isUseTmp);

	float NPplusD = glm::dot(N4, P) + D;
	if(NPplusD > s->getRadius()) {
		return false;
	} else {
		//check for boundry of cube, if it is cube, 
		//but it is ground, which here assumed to be of infinite
    Contact *cont = new Contact;

    cont->p.push_back( s->getCenter(isUseTmp) + -1.0f * N4 * s->getRadius() );
    cont->b = g;
    cont->a = s;
    cont->n = glm::vec4(N,0);
    contacts.push_back(cont);

    return true;
	}

	return false;
}

bool collisionDetection::sphereGround(sphere *s, ground *g, std::vector<Contact*> &contacts, bool inOrder, bool isUseTmp) {
  //HARE KRISHNA
  glm::vec4 pointOnPlane = g->getPointOnPlane();
  glm::vec4 pointToCenter;

  pointToCenter = s->getCenter(isUseTmp) - pointOnPlane;
  
  glm::vec4 planeNormal = glm::vec4(g->getNormal(),0);
  float radius = s->getRadius();
  float projectionVal = glm::dot(pointToCenter,planeNormal); 
  if(abs(abs(projectionVal) <= (radius+.01)) ) {
    //It is colliding
    if(projectionVal < 0) {
            planeNormal = -planeNormal;
    }

    Contact *cont = new Contact;
    cont->p.push_back(s->getCenter(isUseTmp) + -s->getRadius()*planeNormal);
    cont->b = g;
    cont->a = s;
    cont->n = planeNormal;
    contacts.push_back(cont);

    return true;
  }
  return false;
}

//NOTE: with cubeGround we only support position with "xTmp" and "RTmp"
bool collisionDetection::cubeGround(cube* c, ground *g, std::vector<Contact*> &contacts, bool isForCollision, bool isUseTmp) {
  //get any point on ground and normal
  glm::vec4 groundP = g->getPointOnPlane();
  glm::vec3 groundN = g->getNormal();

  //get points of cube
  const std::vector<glm::vec4> &cubePoints = c->getCurrentPositoins(isUseTmp);

  //initialize resultBool as false
  bool resultBool = false;
  std::vector<glm::vec4> intersectPoints;

  //for each point in cube, check whether it is above ground, or below ground
  for(int i=0; i<8; i++) {
    //get vector from groundPoint to cubePoint
    glm::vec3 gToC = (cubePoints[i] - groundP).xyz;

    //project that vector along ground normal
    float distance = glm::dot(gToC, groundN);

    //if project is +ve return continue, i.e. no projection
    if(distance > 0.0001) {
      continue;
    }
    intersectPoints.push_back(cubePoints[i]);
  }

  if(intersectPoints.size()>0) {
    Contact *cont = new Contact();

    if(isForCollision) {//we are storing aggregate of all contact points
    glm::vec4 finalPoint = std::accumulate(intersectPoints.begin(), intersectPoints.end(), glm::vec4(0,0,0,0));
    finalPoint /= (float)intersectPoints.size();
    cont->p.push_back(finalPoint);
 
    } else {//we are storing all contact points
        cont->p = intersectPoints;
    }

    //In case of CUBE-GROUND or of any 2 Convex Surface, normal must be common for all contact points
    //If reached here means it is contacting so, FILL the contacts vector with correspointing values
    cont->n = glm::vec4(groundN, 0);
    cont->a = c;
    cont->b = g;
    contacts.push_back(cont);

    //set resultBool true
    resultBool = true;
  }

  return resultBool;
}

bool collisionDetection::cubeSphere(cube *c, sphere *s, std::vector<Contact*> &contacts, bool isUseTmp) {
  //project sphere's center to cube
  glm::vec4 nearestPoint;
  closestPoint(c, s->getCenter(isUseTmp), nearestPoint, isUseTmp) ;


  //if distance between projected point and sphere's center is > sphere's radius
  glm::vec4 nearestToSphr = s->getCenter(isUseTmp) - nearestPoint;
  if( glm::length(nearestToSphr) > s->getRadius() ) {
    // then return false
    return false;
  }

  //collision normal is direction vector from projected point to sphere's center
  glm::vec4 collisionNormal = glm::normalize(nearestToSphr);

  //collision point is: center - N * radius
  glm::vec4 collisionPoint = s->getCenter(isUseTmp) - collisionNormal * s->getRadius();

  //fill out contact and return true
  Contact* cont = new Contact;
  cont->a = s;
  cont->b = c;
  cont->p.push_back(collisionPoint);
  cont->n = collisionNormal;

  contacts.push_back(cont);
}

bool collisionDetection::cubeCube(cube *c1, cube *c2, std::vector<Contact*> &contacts, bool isForCollision, bool isUseTmp) {
  ///Calculate all potential axis for separation of cubes
  glm::mat3 c1Rot = c1->getRotationMatrix(isUseTmp);
  glm::mat3 c2Rot = c2->getRotationMatrix(isUseTmp);

  glm::vec3 separateAxis[15] = {
    glm::vec3(c1Rot * glm::vec3(1,0,0)), //X-axis in c1
    glm::vec3(c1Rot * glm::vec3(0,1,0)), //Y-axis in c1
    glm::vec3(c1Rot * glm::vec3(0,0,1)), //Z-axis in c1
    glm::vec3(c2Rot * glm::vec3(1,0,0)), //X-axis in c2
    glm::vec3(c2Rot * glm::vec3(0,1,0)), //Y-axis in c2
    glm::vec3(c2Rot * glm::vec3(0,0,1)), //Z-axis in c2
  };
  // Cross-Product of AxB order
  int c1Index = 0;
  for(int i=6; i<15; i+=3) {
    separateAxis[i]   = glm::normalize(glm::vec3( glm::cross(separateAxis[c1Index], separateAxis[3]) ));
    separateAxis[i+1] = glm::normalize(glm::vec3( glm::cross(separateAxis[c1Index], separateAxis[4]) ));
    separateAxis[i+2] = glm::normalize(glm::vec3( glm::cross(separateAxis[c1Index], separateAxis[5]) ));
    c1Index++;
  }

  //*//(A)(Normal of Collision) We always use object "b" as one contains separate Axis
  ///Loop through all axises calculated
  float minOverlap=5000;
  int minAxis=-1;
  bool needFlip = false; //This variable says whether we need to flip our last normal or not
  bool globalIsC1BackSide;
  //i < 6 for only vertex-face, 15 For include edge-edge
  for(int i=0; i<15; i++) { //NOTE::::FOR NOW ONLY i<6, FOR EDGE-EDGE, DO I <15
    float overlap;
    bool isC1BackSide; //This variable say whether our C1 object (about which current axis lies for i<6) is back side
                       //If it is back side, we not need to flip normal else we need to flip it
    //check whether two cubes are overlapping with current axis,
    if( separateAxis[i] != glm::vec3(0) ) {
      if(!checkBoxAxisOverlap(separateAxis[i], c1, c2, overlap, isC1BackSide, isUseTmp)) {
        //if no, return false
        return false;
      }
      // else check and update minimum overlapping distance as well as axis index,
      if(overlap < minOverlap) {
        minOverlap = overlap;
        minAxis = i;
        needFlip = false;
        globalIsC1BackSide = isC1BackSide;
        ////NOTE:= We are not now considering the edge-edge case only for i<6
        //(as in (A)) If isC1BackSide is true and i >= 3 (means C2 is front side) then we need to flip normal
        if(isC1BackSide && i>=3){
          needFlip = true;
        }
        //if isC1BackSide is false and i<3 then we need to flip normals as well
        if(!isC1BackSide && i<3) {
          needFlip=true;
        }
      }
    }
  }

  //Getting here will make sure, two cubes are intersecting
  std::vector<glm::vec4> finalPoint;
  //Calculating correct normal direction
  glm::vec3 finalNormal;

  if(minAxis<0) return false;

  if(minAxis < 6) {
    getPossibleBestPoint(c1, c2, finalPoint,minAxis, globalIsC1BackSide, separateAxis[minAxis], isForCollision, isUseTmp);
    //if(finalPoint.w < 0.9){ //i.e. w==0, means no intersecting point found, see implimentatin of "getPossibleBestPoint" for more detail
    if(finalPoint.size()==0) {
      return false;
    }

    //NOTE: This normal is pointing outword from plane on which we found min. speparating axis
    if(needFlip) {
      finalNormal = -separateAxis[minAxis];
    } else {
      finalNormal = separateAxis[minAxis];
    }

  } else { //minAxis >= 6, which means we have edge-edge intersection
    //*//(Edge-Edge test)We are taking 4 edges of c1 and test it against c2
    //**// For 6=<i<9 take X-axis of c1
    //**// For 9<=i<12 take Y-axis of c1
    //**// For 12<=i<15 take Z-axis of c1
    
    //*//(B)(Normal) in this case "b" will "c2" and "a" will "c1" so, normal always from "b" to "a"
    //*//Need to think for calculating Normal
  
    if(EDGE_TEST_ENABLED) {
      //calculate axisIndex for edges
      int axisIndex;
      if(6  <= minAxis && minAxis < 9) axisIndex=0;
      if(9  <= minAxis && minAxis < 12) axisIndex=1;
      if(12 <= minAxis && minAxis < 15) axisIndex=2;

      //call getEdgeEdgepoint for intersecting point
      std::vector<edge> testingEdges = c1->getCurrentAxisParallelEdge(axisIndex, isUseTmp);
      getEdgeEdgePoint(c2, testingEdges, finalPoint, separateAxis[minAxis], isForCollision, isUseTmp); 

      if(finalPoint.size()==0) {
         return false;
      }

      ////Calculate Normal of collision
      finalNormal = separateAxis[minAxis];
      ///Now need to check it's direction is all right or not, if not then flip it

      //take vector from c2->x to finalPoint[0]
      glm::vec3 c2ToFinalPoint = (finalPoint[0]-c2->x).xyz; 

      //if dot(finalNormal, c2ToFinalPoint) negative then filp finalNormal
      if(glm::dot(finalNormal, c2ToFinalPoint) < 0) {
        finalNormal = -finalNormal;
      }
    } else {
      return false;
    }
    //Get intersection based on Edge-Plane method,
    //So first check, all edge of cube 1 into planes of cube2, if we found any line of intersection,
    //Store two intersection points, and continue, if we found second edge push those two points also
    //Take average of points and return it, no need to test anything
    //THATS ALL :) HARI BOL !! JAY NITAI!!!
  }


  Contact *cont = new Contact;
  cont->p = finalPoint;
  cont->n = glm::vec4(finalNormal,0);
  //Becaouse normal points outword to plane where Separate Axis exist I.E. Object contain plane in collision
  // We always use that object (which has Separate Axis) as object "b"
  if(minAxis<3) {
    cont->b = c1;
    cont->a = c2;
  } 
  if(3<=minAxis && minAxis<6) {
    cont->b = c2;
    cont->a = c1;
  }
  //(as in (B)) for Edge-Edge case b is c2 and a is c1
  if(minAxis>=6) { 
    cont->b = c2;
    cont->a = c1;
  }

  contacts.push_back(cont);
  return true;
}

void collisionDetection::closestPoint(cube *c, const glm::vec4 p, glm::vec4 &pRes, bool isUseTmp) {
  //set pRes equals cube's center
  pRes = c->x;

  //get vector from cube's center to point
  glm::vec3 cubeCToPoint = (p - c->xTmp).xyz;

  //get rotation matrix for cube
  glm::mat3 cubeRot = c->getRotationMatrix(isUseTmp);
  glm::vec3 rotateAxis[3] = { cubeRot*glm::vec3(1,0,0), cubeRot*glm::vec3(0,1,0), cubeRot*glm::vec3(0,0,1) };

  //get box side values for each axis 
  float boxSides[3] = { c->getScaleMatix()[0][0], c->getScaleMatix()[1][1], c->getScaleMatix()[2][2] };

  //for each axis of rotation
    for(int j = 0; j<3; j++) {
      //project the vector on current axis and save as distance
      float distance = glm::dot(rotateAxis[j], cubeCToPoint);

      //if that projected lenght is > halfCubeLenth 
      if(distance > boxSides[j]) {
        //set distance to +halfCubeLength 
        distance = boxSides[j];
      } else if(distance < -boxSides[j]) { //or if < -halfCubeLenth for that cube's axis
        //set distance to -halfCubeLength
        distance = -boxSides[j];
      }
      //set pRes += distance*currentAxisVector
      pRes += distance*glm::vec4(rotateAxis[j],0);
    }
}

bool collisionDetection::checkBoxAxisOverlap(glm::vec3 axis, cube *c1, cube *c2, float &overlap, bool &isC1BackSide, bool isUseTmp) {
  std::pair<float, float> firstRange, secondRange;

  firstRange = getBoxProjection(c1, axis, isUseTmp);
  secondRange = getBoxProjection(c2, axis, isUseTmp);

  if( (firstRange.first > secondRange.second) || (secondRange.first  > firstRange.second) ) {
    //We finally found the separation axis between two cubes
    return false;
  }

  //find the overlapping distance and set it inside 'overlap' variable
  if(firstRange.first < secondRange.first) {
    overlap = firstRange.second - secondRange.first;
    isC1BackSide = true;
  } else { //secondRange.first < firstRange.first
    overlap = secondRange.second - firstRange.first;
    isC1BackSide = false;
  }
  return true;
}

std::pair<float, float> collisionDetection::getBoxProjection(cube *c, glm::vec3 axis, bool isUseTmp) {
  const std::vector<glm::vec4> verts = c->getCurrentPositoins(isUseTmp);
  float min, max ;
  
  min=max = glm::dot(glm::vec4(axis,0),verts[0]);
  for(int i=1; i<8 /*vertexSize*/; i++) {
    float val = glm::dot(glm::vec4(axis,0), verts[i]);    
    if(val < min) {
      min=val;
    }
    if(val > max) {
      max=val;
    }
  }

  return std::pair<float, float>(min, max);
}

void collisionDetection::getPossibleBestPoint(cube *c1, cube *c2, std::vector<glm::vec4> &p, int axisIndex, bool isC1BackSide, glm::vec3 collisionNormal, bool isForCollision, bool isUseTmp) {
  //*//(A)(Edge-Cube Test) We'll test only one 4 edge-Cube test. And those edges are of plane on which 2 cubes colliding.

  //getting all point of cube 1 & 2
  const std::vector<glm::vec4> &points1 = c1->getCurrentPositoins(isUseTmp);
  const std::vector<glm::vec4> &points2 = c2->getCurrentPositoins(isUseTmp);

  //declate list of all intersected points
  std::vector<glm::vec4> intersectPoints;

  //of each points of cube1, check whether it is inside of cube2 and vice versa and push those points in list
  for(int i=0; i<8; i++) {
    if(pointInBox(c1, points2[i], isUseTmp)) {
      intersectPoints.push_back(points2[i]);
    }
    if(pointInBox(c2, points1[i], isUseTmp)) {
      intersectPoints.push_back(points1[i]);
    }
  }
  
  //(old)Because this function only handles FACE-VERTEX case, in case of EDGE-EDGE case, we might not found Separating Axis
  //(as in (A))Checking edges of cube's collision face with other cube
  if(EDGE_TEST_ENABLED) {
    std::vector<glm::vec4> face;
    cube *testingCube;

    //getting face and cube, between which we need to run test
    if(axisIndex<3) {//if collision on cube 1
      //if c1 is back side then get positive side of face else negative side
      face = c1->getCurrentFace(axisIndex, isC1BackSide, isUseTmp);
      testingCube= c2;
    } else {//collision is on cube 2
      //if c1 is back side then get negative side of face else positive side
      face = c2->getCurrentFace(axisIndex-3, !isC1BackSide, isUseTmp);
      testingCube = c1;  
    }

    //loop 4 times to check intersection of 4 edges to testingCube
    for(int i=0; i<4; i++) {
      glm::vec4 point;

      if( lineInBox(testingCube, face[i], face[ (i+1)%4 ], collisionNormal, point, isUseTmp) ) {
        intersectPoints.push_back(point);
      }
    }

  } 
  
  //But here we also not get any intersecting points 
  if(intersectPoints.size()==0) {    
    //p=glm::vec4(0);
    return;
  }

  if(isForCollision) {//get average of intersected points and set to p
    glm::vec4 point = std::accumulate(intersectPoints.begin(), intersectPoints.end(), glm::vec4(0,0,0,0));
    point /= (float)intersectPoints.size();
    p.push_back(point);
  } else {
    p = intersectPoints;
  }
}

void collisionDetection::getEdgeEdgePoint(cube *c, const std::vector<edge> &e, std::vector<glm::vec4> &p, glm::vec3 collisionNormal, bool isForCollision, bool isUseTmp) {
  //declare vector of point to store collision points
  std::vector<glm::vec4> intersectPoints;

  //loop 4 times over all edges
  const int totalEdges = 4;
  for(int i=0; i<totalEdges; i++) {
    glm::vec4 pointOfInter;
    //call lineInBox, if collide store in collisionPoints
    if(lineInBox(c, e[i].first, e[i].second, collisionNormal, pointOfInter, isUseTmp)) {
      intersectPoints.push_back(pointOfInter);
    }
  }

  //check if no of collisionPoints 0 return
  if(DEBUG) {//Something Awekword happeining 
    if(intersectPoints.size()==0) {
      int i=4;
      i*=3;
    }
  }

  if(intersectPoints.size()==0) {
    return;
  }

  //if it's for collision then take average and push back in p else copy list inside p
  if(isForCollision) {
    glm::vec4 point;
    point = std::accumulate(intersectPoints.begin(), intersectPoints.end(), glm::vec4(0));
    point /= (float)intersectPoints.size();
    p.push_back(point);
  } else {
    p = intersectPoints;
  }

}



bool collisionDetection::pointInBox(cube *c, glm::vec4 p, bool isUseTmp) {
  //point to box vector
  glm::vec3 boxToPoint = (p - c->xTmp).xyz;
  
  //get half length of box
  glm::mat4 scale = c->getScaleMatix();
  float boxSideHalf[3] = {scale[0][0], scale[1][1], scale[2][2]};
  //get 3 axis of box
  glm::vec3 axis[3] = { 
    c->getRotationMatrix(isUseTmp)*glm::vec3(1,0,0),
    c->getRotationMatrix(isUseTmp)*glm::vec3(0,1,0),
    c->getRotationMatrix(isUseTmp)*glm::vec3(0,0,1) 
  };
  for(int i=0; i<3; i++) {
    float proj = glm::dot(axis[i],boxToPoint);
    if(proj > boxSideHalf[i] || proj < -boxSideHalf[i]) {
      return false;
    }
  }

  //if reached here point is inside the box
  return true;
}

bool collisionDetection::lineInBox(cube* c, glm::vec4 p1, glm::vec4 p2, glm::vec3 collisionNormal, glm::vec4 &intersectPoint, bool isUseTmp) { 
  //*//Key Concept:= Our inspiration is ray-Box intersection test. Here our ray's direction is non-Unit vector which is (p2-p1). In such case 't' we get, if t>1 then it's beyoun edge.
  
  //*//Key Cases:= any vertex and both vertex(whole line) inside cube. Such case only possible when it strikes over surface plane or Vertex-Plan Intersection.
    //**//We have to consider only one case in which edge is coming out of Non-Collision plan.
    //**//In other case, just return false. Becouse such cases handled via vertex-Face case.

  //*//Inspiration from:= 'Game Physics Cookbook' chapter 10's "Linetest Oriented bounding box" AND CIS 277 2015 "Feb_17_Raycasting_Lecture"

  
  //generate a ray with p1 as origin and (p2-p1) as direction (Withought normalizing it)'
  glm::vec4 direction = p2-p1;

  //get all rotation axis of cube and store in X, Y, Z axis
  glm::mat3 rotationMat = c->getRotationMatrix(isUseTmp);
  glm::vec3 axis[3]= { rotationMat * glm::vec3(1,0,0),
                       rotationMat * glm::vec3(0,1,0),
                       rotationMat * glm::vec3(0,0,1) };

  //also get magnitude of cube's half dimension in X, Y, Z axis
  glm::vec3 halfDim = c->getHalfDimension();

  ////Just like in case of cube at origin, get (p1 - cube.x) for same concept. And store it's projection on X, Y, Z axis of cube's rotation as it's dimention. named "pCube"
  glm::vec3 cubeToP = (p1-c->x).xyz;//***NOTE****In book this is done in reverse
  glm::vec3 pCube (glm::dot(cubeToP, axis[0]),
                   glm::dot(cubeToP, axis[1]),
                   glm::dot(cubeToP, axis[2]));

  
  ////Just like in case of cube at origin, get (p2-p1) projections on X,Y,Z axis of cube's rotation as it's dimention on such axis. named "dCube"
  glm::vec3 p1ToP2 = direction.xyz;
  glm::vec3 dCube ( glm::dot(p1ToP2, axis[0]),
                    glm::dot(p1ToP2, axis[1]),
                    glm::dot(p1ToP2, axis[2]));

  //create a float array 't' with 6 elements for tmin an tmax
  float t[6];
  
  //loop 3 time for 3 axises
  for(int i=0; i<3; i++) {
    //Test whether line is parallel to current slab(NOTE:: Parallel to slab doesn't mean parallel to that axis)
    if(utils::epsilonCheck(dCube[i],0)) {
      //if pCube is not between halfDim[i] then we miss, return false
      if(  pCube[i] < -halfDim[i]|| pCube[i] > halfDim[i] ) {
        return false;
      }
      //set dCube[i] to 0.00001 to avoid divide by zero and make tMin and tMax extreme
     //**//LATER This method fail when (halfDim[i]-pCube[i])=0. Which makes tMax=0 and when tMin=0; It makes such point by passing all tests!!
      //dCube[i] = 0.00001;
      t[i*2+0] = -99999;
      t[i*2+1] =  99999;
    } else {
      //tMin = (-cubeSide - pCube)/dCube
      t[i*2+0] = (-halfDim[i] - pCube[i])/dCube[i]; 
      //tMax = (cubeSide - pCube)/dCube
      t[i*2+1] = ( halfDim[i] - pCube[i])/dCube[i];

      //rearrange value of 't' array so that, 0,2,4 are less than 1,3,5
      if(t[i*2+0]>t[i*2+1]) {
        float tmp = t[i*2+0];
        t[i*2+0] = t[i*2+1];
        t[i*2+1] = tmp;
      }
    }
  }


  //calculate 'tMin' as Maximum of "t"'s minimum (i.e. 0,2,4)
  float tMin = std::max( std::max(t[0],t[2]), t[4] );

  //calculate 'tMax' as Minimum of "t"'s maximum (i.e. 1,3,5)
  float tMax = std::min( std::min(t[1], t[3]), t[5] );

  //=//Cases=> 7 Cases
  //==//Case 1.(NoHit) tMax < tMin, which meanse we miss target. Return false
  if(tMax < tMin) return false;

  //==//Case 2.(Back) tMax < 0, means ray hitting at back side of origin. Return false
  if(tMax < 0) return false;

  //==//(Next cases after ray hitting cube, deals with lenth of ray)
  //==//Case 4.(Inside) tMin<0 and tMax>1, means line completly inside of cube. return false
  if( (tMin<=0) && (tMax>=1) ) return false;
  
  //==//Case 5.(Outside) tMin>1, means line segment is out of cube. return false
  if( (tMin>1) ) return false;

  //==//Case 6.(halfInside) tMin<0 or tMax>1, meanse one point is inside cube and other is outside
  if(tMin<0 || tMax>1) {
    //*//get normal of hitting plan and make dot product with collision plan. If non-zero then return false, else return intersecting point.
    float tEvaluate = (tMin<0 ? tMax : tMin);
    glm::vec3 normal(0);
    glm::vec4 linePoint = p1 + tEvaluate * direction;
    glm::vec3 centerToP = (linePoint - c->x).xyz;

    //loop through each axis of cube
    for(int i=0; i<3; i++) {
      //take dot product with current axis and store as 'project'
      float project = glm::dot(centerToP, axis[i]); 

      //check if abs('project') equals halfDim of that axis
      if( utils::epsilonCheck(abs(project),halfDim[i]) ) {
        //if 'project' is positive then store normal as current axis, else -ve current axis
        normal = (project>0 ? axis[i] : -axis[i]);
        //break from loop
        break;
      }
    }

    if(DEBUG) { //checking wheter normal is still zero???
      if(normal == glm::vec3(0)) {
        int i=4;
        i *=3;
      }
    }

    //Now, if dot('normal', collisionNormal) non zero i.e. it's abs()>0.5, return false
    float dotValue = glm::dot(normal, collisionNormal); 
    
    if(DEBUG) {//just checking normal is either parallel or perpendicular to coliision plan
      if(utils::epsilonCheck(dotValue,0)) {
      } else if (utils::epsilonCheck(dotValue,1)) {
      } else {
        //COMING HERE PROVES THAT, COLLISION NORMAL IS EDGE-EDGE. And it's not much possible for such case to have one point inside another cube!!!!!!!!!
        int i=0;
        i*=4;
      }
    }
    
    if( abs(dotValue) > 0.5 ) {
      return false;
    }

    //store this point and return true
    intersectPoint = linePoint;
    return true;
  }

  //==//Case 7.(EdgeEdge)(default) tMax>0 and tMin>0. return point on (tMin+tMax)/2.
  if(DEBUG) {//check of any case mission with tMax and tMin
    if(tMax>0 && tMin>0 && tMax<1){
    } else {
      int i=4;
      i*=3;
    }
  }
  float tAvg = (tMin+tMax)/2;
  intersectPoint = p1 + tAvg * direction;
  return true;

}
