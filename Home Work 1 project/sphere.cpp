#include "sphere.h"
#include<iostream>
#include <glm\glm.hpp>
#include <glm\gtc\matrix_transform.hpp>

#include "textureUtils.h"
#include "utils.h"

#define PI 3.141592653589f
#define BUFFER_OFFSET(val) (void*)val

const int noOfHorizontalSection = 24; // 360/15=24 // For UV Mapping
const int totalIdx = 1585;//33606; //From initializeIdx => 71 + 10*23*6 + 23*3 + 3	
const int posCount = 11*24 + 2*noOfHorizontalSection; //verticle * horizontal + (top+bottom)

void initializePos (glm::vec4 (&pos)[posCount]);
void initializeNor (glm::vec4 (&nor)[posCount]);
void initializeIdx (unsigned int (&idx)[totalIdx]);
void initializeTextureUV( glm::vec2 (&texUV)[posCount]);


sphere::sphere(double inMass, double inCoeffFrictoin) : rigidBody(inMass, inCoeffFrictoin) {
	x = glm::vec4(0, 0, 0, 1); //initial position
	v = glm::vec4(0, 0, 0, 0); //initial velocity

  omega = glm::vec3(0, 0, 0);
	_scale = glm::vec3(1, 1, 1);

  //initial inertial of body
  // 2/5 * M * R*R * IdentityMatrix
  Ibody = (float)(2.0/5.0 * getMass() * 1) * glm::mat3(1);
 
  Ibodyinv = glm::inverse(Ibody);

	bodyType = rigidBody::SPHERE;
}


void sphere::create()
{
	//initialize count
	count = totalIdx;

	///create array to store index, position and normal
	glm::vec4 pos[posCount];
	glm::vec4 nor[posCount];
	unsigned int idx[totalIdx];
  glm::vec2 texUV[posCount];

	///initialize above arrays by calling their routines
	initializePos(pos);
	initializeNor(nor);
	initializeIdx(idx);
  initializeTextureUV(texUV);


	//send position
	glGenBuffers(1, &bufPos);
	glBindBuffer(GL_ARRAY_BUFFER, bufPos);
	glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec4)*posCount, pos, GL_DYNAMIC_DRAW);

	//send normal
	glGenBuffers(1, &bufNor);
	glBindBuffer(GL_ARRAY_BUFFER, bufNor);
	glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec4)*posCount, nor, GL_STATIC_DRAW);

	//send index
	glGenBuffers(1, &bufIdx);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, bufIdx);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned int)*totalIdx, idx, GL_STATIC_DRAW);

  //send UV
  glGenBuffers(1, &bufUV);
  glBindBuffer(GL_ARRAY_BUFFER, bufUV);
  glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec2)*posCount, texUV, GL_DYNAMIC_DRAW);
}

sphere::~sphere(void)
{
}

GLuint sphere::idxCount() {
	return count;
}

GLboolean sphere::bindIdx() {
  if(glIsBuffer(bufIdx)) {
	  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, bufIdx);
    return true;
  }
  return false;
}

GLboolean sphere::bindPos() {
  if( glIsBuffer(bufPos) ) {
	  glBindBuffer(GL_ARRAY_BUFFER, bufPos);
    return true;
  }
  return false;
}

GLboolean sphere::bindNor() {
  if (glIsBuffer(bufNor) ) {
	  glBindBuffer(GL_ARRAY_BUFFER, bufNor);
    return true;
  }
  return false;
}

GLboolean sphere::bindUV() {
  if( glIsBuffer(bufUV) ) {
    glBindBuffer(GL_ARRAY_BUFFER, bufUV);
    return true;
  }
  return false;
}

GLboolean sphere::bindTexture() {
  if(glIsTexture(textureID)) {
    glBindTexture(GL_TEXTURE_2D, textureID);
    return true;
  }
  return false;
}

void initializePos (glm::vec4 (&pos)[posCount]) {
  //PHI :- Measured from +y axis
  //THETA :- Measured from Y-Z plane
	int count = 0;

  for(int i=0; i<noOfHorizontalSection; i++) {
	  pos[count++] = glm::vec4(0,1,0,1); //topest point
  }

	for(int theta = 15; theta<180; theta+=15) {
		for(int phi = 0; phi<360; phi+=15) {
			pos[count] = glm::vec4( sin(PI/180.0 * phi) * sin(PI/180.0 * theta), //x = cos(phi)*sin(theta)
							   cos(PI/180.0 * theta), //y = cos(phi)*cos(theta)
							   cos(PI/180.0 * phi) * sin(PI/180.0 * theta), 1.0);					//z = sin(phi)
			count++;
		}
	}
  for(int i=0; i<noOfHorizontalSection; i++) {
	  pos[count++] = glm::vec4(0,-1,0,1); //downest point
  }
  printf("sphere debug");

}
void initializeNor (glm::vec4 (&nor)[posCount]) {
  //PHI :- Measured from +y axis
  //THETA :- Measured from Y-Z plane

	int count = 0;

  for(int i=0; i<noOfHorizontalSection; i++) {
	  nor[count++] = glm::vec4(0,1,0,1); //topest point
  }

	for(int theta = 15; theta<180; theta+=15) {
		for(int phi = 0; phi<360; phi+=15) {
			nor[count] = glm::vec4( sin(PI/180.0 * phi) * sin(PI/180.0 * theta), //x = cos(phi)*sin(theta)
							   cos(PI/180.0 * theta), //y = cos(phi)*cos(theta)
							   cos(PI/180.0 * phi) * sin(PI/180.0 * theta), 1.0);					//z = sin(phi)
			count++;
		}
	}
  
  for(int i=0; i<noOfHorizontalSection; i++) {
    nor[count++] = glm::vec4(0,-1,0,1); //downest point
  }

}
void initializeIdx (unsigned int (&idx)[totalIdx]) {
  int indexObstacle = noOfHorizontalSection - 1;//Because, first noOfHorizontalSection values, we use for north pole

	int count=0;
	//UPPER FAN
	for(int i=0; i<23; i++) { //for 23 point instead of 24 point
		idx[i*3 + 0] = i;
		idx[i*3 + 1] = i+1 + indexObstacle; //Because first noOfHorizontalSection memory for north pole
		idx[i*3 + 2] = i+2 + indexObstacle; ////Because first noOfHorizontalSection memory for north pole
		count += 3;
	}
	//for 24th point
	/*idx[69] = 0; 
	idx[70] = 1;
	idx[71] = 24;*/
	idx[count] =  indexObstacle;
  idx[count+1] = 1 + indexObstacle;
	idx[count+2] = 24 + indexObstacle;
	count+=3;

	//int count = 72;
	//PART BETWEEN LOWER AND UPEER FAN
	for(int j=0; j<10; j++) {
		for(int i=1; i< (1+23); i++) { //using 23 not 24 because of last boundry case where triangles are (48,25,72) and (25,72,49) for first one
			idx[count] = j*24 +  i + 0 + indexObstacle;
			idx[count+1] = j*24 + i + 1 + indexObstacle;
			idx[count+2] = j*24 + i + 24 + indexObstacle;
			idx[count+3] = j*24 + i + 1 + indexObstacle;
			idx[count+4] = j*24 + i + 25 + indexObstacle;
			idx[count+5] = j*24 + i + 24 + indexObstacle;
			count += 6;
		}
		//For last Rectangle for each horizontal strip
		idx[count] = j*24 + 24 + indexObstacle;
		idx[count+1] = j*24 + 1 + indexObstacle;
		idx[count+2] = j*24 + 48 + indexObstacle;
		idx[count+3] = j*24 + 1 + indexObstacle;
		idx[count+4] = j*24 + 25 + indexObstacle;
		idx[count+5] = j*24 + 48 + indexObstacle; 
		count += 6;
	}

	//Lower Fan
	for(int i=0; i<23; i++) {
    idx[count] = posCount-1 - (indexObstacle-i); //Last index - (-23 to -1)
		idx[count+1] = 241 + i  + indexObstacle; //265 => j*24 + 25 + 24 where j=9
		idx[count+2] = 241 + i + 1 + indexObstacle;
		count += 3;
	}

	idx[count] = 264 + indexObstacle;//posCount-1;
	idx[count+1] = 241 + indexObstacle; //256 + 23
	idx[count+2] = posCount-1;
	count += 3;
}

void initializeTextureUV( glm::vec2 (&texUV)[posCount]) {
  //PHI :- Measured from +y axis
  //THETA :- Measured from Y-Z plane
	int count = 0;
  float horizontalStep = 1.0/noOfHorizontalSection;
  float verticalStep = 1.0/12;

  for(int i=0; i<noOfHorizontalSection; i++) {
	  texUV[count++] = glm::vec2(i*horizontalStep, 1); //topest point
  }

	for(int theta = 1; theta<12; theta++) {
		for(int phi = 0; phi<noOfHorizontalSection; phi++) {
      texUV[count] = glm::vec2(phi*horizontalStep, 1.0f-theta*verticalStep );
			count++;
		}
	}
  for(int i=0; i<noOfHorizontalSection; i++) {
	  texUV[count++] = glm::vec2(i*horizontalStep, 0); //downest point
  }
}

void sphere::setScale(float x, float y, float z) {
	_scale = glm::vec3(x, y, z);

  if(getMassInv() != 0) {
    //initial inertial of body
    // 2/5 * M * R*R * IdentityMatrix
    Ibody = (float)(2.0/5.0 * getMass() * getRadius()*getRadius()) * glm::mat3(1);
 
    Ibodyinv = glm::inverse(Ibody);

  }
}

void sphere::setRadius (float inRadius) {
  _scale = glm::vec3(inRadius, inRadius, inRadius);
}

void sphere::setTexture(const char* fileName) {
  int width, height;
  GLubyte *image;
   textureUtils::loadBMPTexture(fileName, (unsigned char **)&image, &width, &height);
  CHECK_OGL_ERROR
  //Generating texture data in GPU
  glGenTextures(1, &textureID);
  glBindTexture(GL_TEXTURE_2D, textureID);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_BGR, GL_UNSIGNED_BYTE, image);
  delete []image;
  CHECK_OGL_ERROR
  //Setting wrapping/filtering option for currently bounded texture
  //Now set how you want to use this across U and V direction, because glBindTexture first time set default state
  //1. Repetaion of image across U and V direction if U/V value will > 1 or < 0
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_MIRRORED_REPEAT);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_MIRRORED_REPEAT);

  //2. Set filter for Minification and Magnification
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);//gl_linear, take average of near pixel to avoid anialiasing
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

  CHECK_OGL_ERROR
}

glm::mat4 sphere::getScaleMatrix() {
	return glm::mat4( _scale[0] , 0			, 0			, 0,
					  0			, _scale[1]	, 0			, 0,
					  0			, 0			, _scale[2]	, 0,
					  0			, 0			, 0			, 1);
}

float sphere::getRadius() {
	return _scale[0];
}

glm::vec4 sphere::getCenter(bool isUseTmp) {
  if(isUseTmp) {
    return xTmp;
  } else {
  	return x;
  }
}

glm::mat4 sphere::getModelMatrix(bool isUseTmp) {
  if(isUseTmp) {
	  return glm::mat4( 1		, 0		, 0		, 0,
					            0 	, 1		, 0		, 0,
					            0		, 0		, 1		, 0,
              xTmp[0]	, xTmp[1]	, xTmp[2]	, 1) * utils::mat3ToMat4(RTmp) * getScaleMatrix();

  } else {
	  return glm::mat4( 1		, 0		, 0		, 0,
					            0		, 1		, 0		, 0,
					            0		, 0		, 1		, 0,
              x[0]	, x[1]	, x[2]	, 1) * utils::mat3ToMat4(R) * getScaleMatrix();
  }

}