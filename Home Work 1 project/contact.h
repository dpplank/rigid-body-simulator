#ifndef CONTACT_H
#define CONTACT_H

#include "rigidBody.h"

//NOTE: because we are having cyclic dependency between Contact and RigidBody
//      class, we need to declare rigidBody in here
class rigidBody;

typedef struct Contact {
	rigidBody *a, *b;

	std::vector<glm::vec4> p;
	glm::vec4 n;

  Contact():a(nullptr),b(nullptr), n(glm::vec4(0)){} 
} Contact;


#endif //CONTACT_H