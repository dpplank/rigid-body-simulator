#include "shader.h"
#include <iostream>
#include <fstream>
#include <vector>

#include "utils.h"

using namespace std;

struct shaderData {
	GLenum type;
	const char* name;
	char * source;
};

GLchar *fileToChar (const char *fileName);

static char* shaderData1(const char* fileName);

shader::shader() {

}

void shader::create(const char *vShader, const char *fShader)
{
	//initialize shader data
	shaderData sData[2] = 
	{{GL_VERTEX_SHADER, vShader, NULL},
	 {GL_FRAGMENT_SHADER, fShader, NULL}};
	//initialize shader program to empty program
	program = glCreateProgram();
	
	//loop for every shader
	for(int i=0; i<2; i++) {		
		//assign source
		sData[i].source = fileToChar(sData[i].name);
		cout<<"code for i = "<<i<<" is: \n"<<sData[i].source<<endl;

		//allocate shader object
		GLuint shaderObj = glCreateShader(sData[i].type);
		//associate sorce of shader to just created shader object
		const GLchar *source[] = {sData[i].source};
		glShaderSource(shaderObj, 1, source, NULL);	
		//free up memory of source file
		delete []sData[i].source;
		//compile this shader object
		glCompileShader(shaderObj);
		
		//check for any error
		GLint status;
		glGetShaderiv(shaderObj, GL_COMPILE_STATUS, &status);
		if(!status) {	//if status is not good
			//get information about error
			GLint size;
			glGetShaderiv(shaderObj, GL_INFO_LOG_LENGTH, &size);
			char *infoLog = new char[size];
			glGetShaderInfoLog(shaderObj, size, NULL, infoLog);
			cerr<<infoLog<<endl;
			
			delete []infoLog;
      system("pause");
		}

		//attach this shader object to our core shader program
		glAttachShader(program, shaderObj);
	}
	//link the program
	glLinkProgram(program);
	//use the program as being current, just for default behavior
	glUseProgram(program);

	cout<<"program: "<<program<<endl;

	//Initialize all shader variable location
	vPos = glGetAttribLocation(program, "vsPosition");
	vNor = glGetAttribLocation(program, "vsNormal");
  vUV = glGetAttribLocation(program, "vsUV");

	unifModel = glGetUniformLocation(program, "uModelMat");
	unifInvModel = glGetUniformLocation(program, "uInvTranModelMat");
	unifViewProj = glGetUniformLocation(program, "uViewProjMat");
	unifColor = glGetUniformLocation(program, "uColor");
  //unifTextureID1 = glGetUniformLocation(program, "texture1");

  //Because we are using one texture per Object, we are unabling Texture Unit 0
  //glActiveTexture(GL_TEXTURE0);
  //glUniform1i(unifTextureID1, 0);

	cout<<"vPos: "<<vPos<<endl;
	cout<<"vNor: "<<vNor<<endl;
  cout<<"vsUV: "<<vUV<<endl<<endl;

	cout<<"uModel: "<<unifModel<<endl;
	cout<<"uInvModel: "<<unifInvModel<<endl;
	cout<<"uViewProj: "<<unifViewProj<<endl;
	cout<<"uColor: "<<unifColor<<endl;

  printf("debug");
}

void shader::loadModelMat(glm::mat4 modelMat = glm::mat4(1.0)) {

  useThis();
	//Load the model matrix
	glUniformMatrix4fv(unifModel, 1, GL_FALSE, glm::value_ptr(modelMat)	);

	//Create matrix for normal transformation which is
	//BECAUSE IT IS RIGID BODY, SO ORIGINAL TRANSFORMATION CAN BE USE TO 
	//TRANSFORM NORMAL TOO, OTHER ONLY REQUIRE IF WE HAVE SCALE ON OBJECT
	glUniformMatrix4fv(unifInvModel, 1, GL_FALSE, glm::value_ptr(modelMat));

}

void shader::loadViewProj(glm::mat4 viewProjMat = glm::mat4(1.0f)) {
	//Load view proj matrix
	//viewProjMat = glm::mat4(1);
  useThis();
	glUniformMatrix4fv(unifViewProj, 1, GL_FALSE, glm::value_ptr(viewProjMat));

}

void shader::loadUnifColor(glm::vec4 inColor = glm::vec4(1,1,0,1)) {
	//load uniform color
	glUniform4fv(unifColor, 1, glm::value_ptr(inColor));
}

void shader::draw(DrawObject &d, shader::drawType inDrawType) {
	useThis();//FOR DEBUG TEXTURE, DISABLING

	///For each Vertex Buffers Do following steps
	//1. bind the buffer
	//2. enable vertex attribute pointer
	//3. Set vertex attribute pointer
  //For position
	if(d.bindPos() && (vPos != -1)) {
		glEnableVertexAttribArray(vPos);
		glVertexAttribPointer(vPos, 4, GL_FLOAT, GL_FALSE, 0, NULL);
  }
  //For normal //FOR DEBUG TEXTURE, DISABLING
	if(d.bindNor() && (vNor != -1)) {
	//	glEnableVertexAttribArray(vNor);
	//	glVertexAttribPointer(vNor, 4, GL_FLOAT, GL_FALSE, 0, NULL);
  }
  //For UV coordinate
  if(d.bindUV() && (vUV != -1)) {
    glEnableVertexAttribArray(vUV);
    glVertexAttribPointer(vUV, 2, GL_FLOAT, GL_FALSE, 0, NULL);

    //For texture associated with geometry
    d.bindTexture();
  }

	///Then bind object's Index and draw it
	d.bindIdx();
  if(inDrawType == shader::TRIANGLE) {
	  glDrawElements(GL_TRIANGLES, d.idxCount(), GL_UNSIGNED_INT, 0);
 
  } else if(inDrawType == shader::drawType::LINE) {
    glDrawElements(GL_LINE_STRIP, d.idxCount(), GL_UNSIGNED_INT, 0);
  }

	///Finally disable All attribute pointer
  if(d.bindPos() && (vPos != -1)) {
	  glDisableVertexAttribArray(vPos);
  }
	//glDisableVertexAttribArray(vNor);
  if(d.bindUV() && (vUV != -1)) {
    glDisableVertexAttribArray(vUV);
  }
}

void shader::useThis() {
	glUseProgram(program);
}

shader::~shader(void)
{
}


GLchar *fileToChar (const char *fileName) {
	std::ifstream inFile(fileName);
	/* std::vector<char> buffer; This are alternative way on doing this but it through some problem in compiling
	buffer.clear();
	buffer.reserve(500); */

	//inFile.seekg(0, inFile.end);
	int counter;
	//counter = inFile.tellg();
	for (counter = 0; inFile.get() != EOF; counter++); 
	inFile.clear(); //clear EOF state
	inFile.seekg(0, inFile.beg); //return to begining of file
	char *bufferChar = new char[counter+1];

	inFile.read(bufferChar, counter);
	/*while(!inFile.eof()) {
		c=inFile.get();
		buffer.push_back(c);
	}
	for(int i=0; i< buffer.size();  i++) {
		bufferChar[i] = buffer[i];
	}*/

	bufferChar[counter] = '\0';
	
	inFile.close();
	return bufferChar;
}
	
static char* shaderData1(const char* fileName){
	cout<<121<<endl;	//
	cout<<fileName<<endl;
	FILE *fp = fopen(fileName,"r");
	
	if( fp==NULL){
		cerr<<"Unable to open file";
		return NULL;
	}
	cout<<112<<endl;	//
	int totalChar;
	for( totalChar=0;getc(fp)!=EOF;totalChar++);
	fseek(fp,SEEK_SET,0);
	char *buffer=new char[totalChar+1];
	fread(buffer,1,totalChar,fp);
	buffer[totalChar]='\0';
	fclose(fp);
	return buffer;
}
