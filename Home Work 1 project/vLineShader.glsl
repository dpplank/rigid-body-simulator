/* hare krsna hare krsna
krsna krsna hare hare */

#version 450

//in stream data
in vec4 vsPosition;
in vec4 vsNormal;

//Uniform Variables
uniform mat4 uModelMat;
uniform mat4 uInvTranModelMat; // For normal transformation 
uniform mat4 uViewProjMat; 
uniform vec4 uColor;

//out stream data
out vec4 fsNormal; // to pass to fragment shader
out vec4 vColor; // debugging

void main () {

	vColor = vec4(1, 0, 0, 1);

	//Dealing with position
	vec4 worldPos = uModelMat * vsPosition;
	
	gl_Position =  uViewProjMat * worldPos; //uViewProjMat * worldPos;

}