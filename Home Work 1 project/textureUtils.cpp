#include "textureUtils.h"
#include <GL/glew.h>
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <string>
#include <streambuf>

using namespace std;

//Predefined values for BMP file
const unsigned int BMPInfoSize = 54;
const unsigned int BMPWeightLoc = 18;
const unsigned int BMPHeightLoc = 22;

textureUtils::textureUtils(void)
{
}


textureUtils::~textureUtils(void)
{
}


void textureUtils::loadBMPTexture(const char* fileName, unsigned char** imageRaw, int *width, int *height) {
  ifstream file;
  file.open(fileName, ios::binary);
  if(!file.is_open()) {
    cerr<<"INVALID FILE PATH\n";
    system("pause");
  }

  char info [BMPInfoSize];
  file.read(info, BMPInfoSize);

  printf("info is \n:", info);

  *width = *(int*)&info[BMPWeightLoc];
  *height = *(int*)&info[BMPHeightLoc];
  printf("Width: %d, Height: %d\n", *width, *height);
  int size = 3 * (*height) * (*width); //3 for 1 byte for each Red, green, blue
  *imageRaw = new unsigned char[size];
  file.read((char*)*imageRaw, size);
  file.close();
  
    for(int i = size - 3; i < size; i += 3)
    {
            printf("Color value for %d is %d, %d, %d\n",i, (*imageRaw)[i], (*imageRaw)[i+1], (*imageRaw)[i+2]);
    }
}

