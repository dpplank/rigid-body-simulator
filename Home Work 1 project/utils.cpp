#include "utils.h"
#include <stdio.h>
#include <stdlib.h>
#include <ctime>
#include <GL/glew.h>
#include <string.h>
#include <iostream>

#define EPSILONF 0.001

utils::utils(void) {
}


utils::~utils(void) {
}

glm::mat4 utils::mat3ToMat4 (glm::mat3 mat) {
	return glm::mat4(mat[0][0], mat[0][1], mat[0][2], 0,
					 mat[1][0], mat[1][1], mat[1][2], 0, 
					 mat[2][0], mat[2][1], mat[2][2], 0, 
							 0,			0,		   0, 1);

}

glm::mat3 utils::vecCrossVecTranspose(glm::vec3 vec) {
	return glm::mat3(vec.x*vec.x, vec.x*vec.y, vec.x*vec.z,
					 vec.y*vec.x, vec.y*vec.y, vec.y*vec.z,
					 vec.z*vec.x, vec.z*vec.y, vec.z*vec.z);
}

glm::mat3 utils::giveVecStar(glm::vec3 v) {
		return glm::mat3(		0,	v[2],	-v[1],
						        -v[2],	   0,	 v[0],
					        	 v[1], -v[0],		0);
}

glm::vec4 utils::velocityAtPoint(rigidBody *body, glm::vec4 point, bool isUseTmp) {
  glm::vec4 r;
  if(isUseTmp) {
    r = point - body->xTmp;
  } else {
    r = point - body->x;
  }
  glm::vec3 omegaInRad = body->getOmegaRad();
  glm::vec4 velocity = body->v + glm::vec4(glm::cross(omegaInRad, glm::vec3(r[0], r[1], r[2])), 0.0);
	
	return velocity;
}

bool utils::epsilonCheck(float lhs, float rhs) {
  float absLhs = fabs(lhs);
  float absRhs = fabs(rhs);
  return (fabs(absLhs - absRhs) < EPSILONF);
}

float utils::getRandomNo(float min, float max) {
  //time_t time;
  //ctime(&time);
  static int count = 0;
  count++;
  int val = count<<434;
  val = val ^ count;
  val = val | count;
  //val = !val;
  srand(time(0) + val);
  int randomVal = rand();
  randomVal = randomVal % 1000 + 1; //its from 0 to 1000
  float randomF = randomVal / 1000.0f; // from 0 to 1
  randomF = randomF * (max - min); //in range of max-min
  return min + randomF;
}

void utils::checkOpenglError(char *file,int line) {
  char c = '\\';
  char * index = strrchr(file, c);
  index++;
  
  GLenum error =  glGetError();

  while(error != GL_NO_ERROR) {
    switch( error ) {
    case (GL_INVALID_ENUM) : {
      printf("GL_INVALID_ENUM, at line no. %d, in flie %s\n", line, index);
      break;
      }
    case (GL_INVALID_VALUE) : {
      printf("GL_INVALID_VALUE, at line no. %d, in flie %s\n", line, index);
      break;
      }
    case(GL_INVALID_OPERATION): {
      printf("GL_INVALID_OPERATION, at line no. %d, in flie %s\n", line, index);
      break;
      }
    case(GL_INVALID_FRAMEBUFFER_OPERATION): {
      printf("GL_INVALID_FRAMEBUFFER_OPERATION, at line no. %d, in flie %s\n", line, index);
      break;
      }
    case(GL_OUT_OF_MEMORY): {
      printf("GL_OUT_OF_MEMORY, at line no. %d, in flie %s\n", line, index);
      break;
      }
    case(GL_STACK_UNDERFLOW): {
      printf("GL_STACK_UNDERFLOW, at line no. %d, in flie %s\n", line, index);
      break;
      }
    case(GL_STACK_OVERFLOW): {
      printf("GL_STACK_OVERFLOW, at line no. %d, in flie %s\n", line, index);
      break;
      }
   default: {
        printf("UNSPECIFED ERROR, at line no. %d, in flie %s\n", line, index);
      }
    }
    system("pause");
    error =  glGetError();
  }

}

glm::vec3 utils::getRotation(glm::vec3 vec, glm::vec3 axis, float angleInDegree) {
  float a = axis[0], b = axis[1], c= axis[2];
  float theta = angleInDegree * PI / 180; //angle In radian
  glm::mat3 R = glm::mat3(
    a*a + (1 - a*a)*cos(theta),         a*b*(1-cos(theta)) - c*sin(theta),  a*c*(1-cos(theta)) + b*sin(theta),
    a*b*(1-cos(theta)) + c*sin(theta),  b*b + (1 - b*b)*cos(theta),         b*c*(1-cos(theta)) - a*sin(theta),
    a*c*(1-cos(theta)) - b*sin(theta),  b*c*(1-cos(theta)) + a*sin(theta),  c*c + (1 - c*c)*cos(theta) 
    );

  //Because glm stores Column major matirix
  R = glm::transpose(R);

  return R*vec;
}

void utils::topologicalSort(std::set<rigidBody*> &nodes, std::vector<std::pair<rigidBody *, rigidBody *>> &edges, std::vector< std::vector<rigidBody*> > &outNodes) {
  //Topological sorting based on this Tutorial : https://youtu.be/PLWoF3qHP74

  //create another graph for processing
  std::set<rigidBody*> procNode(nodes.begin(), nodes.end());
  std::vector<std::pair<rigidBody*, rigidBody*>> procEdge(edges.begin(), edges.end());

  int totalItr =0;
  //do while loop untill we have processed all nodes
  while(procNode.size()>0) {
    totalItr++;
    //Initialize list of all nodes which has no edges coming inside
    std::vector<rigidBody*> noInsideNode;

    //Find all nodes which has no edges coming inside it, I.E. not at edges.second
    for(auto itr=procNode.begin(); itr!=procNode.end(); itr++) {
      bool found = true;
      for(int j=0; j<procEdge.size(); j++) {
        if(procEdge[j].second == *itr) {
          found = false;
          break;
        }
      }
      if(found){
        noInsideNode.push_back(*itr);
      }
    }

    //iterate through all nodes in this level
    for(int i=0; i<noInsideNode.size(); i++) {
      //set level in current object of noInsideNode as size() of outNodes
      noInsideNode[i]->level = outNodes.size();

      //delete every edges corresponding to this node from procEdge
      //NOTE: need to process erase from reverse order
      for(int j=procEdge.size()-1; j>=0; j--) {
        if(procEdge[j].first == noInsideNode[i]) {
          procEdge.erase(procEdge.begin()+j);
        }
      }
      //remove it from procNode
      procNode.erase(noInsideNode[i]);
    }

    //push back it inside outNodes
    outNodes.push_back(noInsideNode);

    if(totalItr > nodes.size()) {
      //std::cout<<"We got loop in DAG. Means Infinite loop"<<std::endl;
      //std::cout<<"Pausing system in Utils.cpp at line no 184"<<std::endl;
      //system("pause");
      std::vector<rigidBody*> remainBodies(procNode.begin(), procNode.end());
      outNodes.push_back(remainBodies);
      break;
    }
  }

}

float utils::setPrecision(float num, int precision) {
  int val = num*pow(10, precision);
  return val/powf(10,precision);
}

glm::mat3 utils::giveRodrignesRot(float angle, glm::vec3 axis) {
	return (float)cos((PI/180.0) * angle) * glm::mat3(1) + (float)sin((PI/180.0)*angle) * utils::giveVecStar(axis) + (float)(1 - cos((PI/180.0)*angle))*utils::vecCrossVecTranspose(axis);
	//return glm::mat4(1);
}


void utils::fillTmpStates(std::vector<tmpStateCache>& states, rigidBody *b, int index, float dt) {

      states[index].xTmp = b->x + b->v * dt;;

      float omegaAbs = glm::dot(b->omega, b->omega);
      if( (omegaAbs > 0.01)) { //by 'false' we disabling rotating	   
        states[index].RTmp = giveRodrignesRot(glm::length(b->omega * (float)dt), b->omega/glm::sqrt(omegaAbs)) * b->R; 
      } else {
        states[index].RTmp = b->R;
      }

}
