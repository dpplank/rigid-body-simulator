//THIS FILE CONTAIN ALL TO DO SUGESSTIONS

//DONE SUGGESTIONS==>
//1// //I. //i.) := I did this by making sceneStructure class. 

//TODOS==>
//1//. DESIGN LEVEL SUGESSTIONS
//I. makeing a class responsible to setup a scene and render that scene
  //i. this also provide function which fillup other vector of pointer of 
  // rigid bodies, such as one contain in "simulation" class

//II. IN CollisionDetection class, into "discoverCollision" function, instead of
// sending position and normal to fill, just send contact list, just like "cubeGround"
// function and add contact in collision detection routins such as "cubeGround" function

//III. Changing name of "cube" class into "box", because, cube represents box with all sides
//   equals.


//2//. FEATURES LEVEL SUGESSTIONS
//1. Currently it not handle the case (such as circularly arranged Dominoes),  because in that case we will
//   get Directed Acyclic Graph with cycles.
    //i. As described in paper "Nonconvex Rigid Bodies with Stacking" section 8.1, first paragraph, we have to 
    //   consider any cycle as all nodes with same level
    //ii. OPTIMIZATION: can be achieved by looking at how many objects we have in current level and scale the 
    //    iteration loop accordingly, i.e. for larger group we have larger iterations.
        //i) but there can be lots of object not touching each other at level 0, so we can make some smart algorithm
        // which only increase iteration when all objects are connected with each other like Dominoes in circular arrangement


//3//. OPTIMIZATION LEVEL SUGESSTIONS
//I. Any acceleration struction in collisionDetection class's "discoverCollisoin" function
//   to reduce no of collision routines
