#pragma once
#include "cube.h"

class ground : public cube
{
public:
	ground(void);
	virtual ~ground(void);

	void setDimension(float width, float height);
	glm::vec3 getNormal();
	glm::vec4 getPointOnPlane();
	glm::mat4 getModelMatrix();
};

