#pragma once
#include <GL\glew.h>
#include "camera.h"
#include "sceneStructure.h"

class mouseHandling
{
public:
	mouseHandling(void);
	~mouseHandling(void);

  static void mouseClicking(GLint button, GLint state, GLint x, GLint y);
	static void mouseDragging(GLint x, GLint y);

  ///SETTER functions
	void setCamera(camera *inCamera);
  void setScene(sceneStructure * inScene);
  //To set Ctrl click, on this bases we can define extra click functionality
  void setCtrlClick(bool val);

  ///GETTER functions
  bool getCtrlClick();

private:

	//Variables for camera manipulation
	static GLint oldX, oldY;
	static bool isDragging;
	static bool isZooming;
	static GLint oldTime;

	//Camera
	static camera *currentCamera;
  //State variable, suggest whether Ctrl is clicked or not
  static bool isCtrlClick;
  //Scene structure, for modify scene using mouse
  static sceneStructure* scene;
  
};

