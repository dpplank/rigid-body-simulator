#pragma once
#include <vector>
#include "shader.h"
#include "rigidBody.h"
#include "simulation.h"

class sceneStructure
{
public:
  sceneStructure(void);
  //Setup the scene
  void createScene();
  //render the scene
  void renderScene(shader &renderingShader, shader::drawType drawType);
  //copy all the scene in provided vector
  void copyAllbodies(std::vector<rigidBody*> &destBodies);
 
  //Shoot sphere at given location and given direction
  void shootSphere(glm::vec4 pos, glm::vec4 direction);

  //SETTER function
  void setSimulation(simulation *inSim);

  ~sceneStructure(void);


private:
  std::vector<rigidBody *> bodies;

  //Pointer to simulation function, in the case if we need to add
  //more bodies in real time
  simulation *mainSimulation;

  ///Scene
  void dominoesWithCubeEnd();
  void twoSimpleCube();
  void domenoesTypeCubes();
  void proceduralDomenoes();
  void edgeTestCube();
  void proceduralTower();
  void blastProblem();
};

