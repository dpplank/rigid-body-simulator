#pragma once
#include <vector>

#include "shader.h"
#include "rigidBody.h"

typedef std::pair<glm::vec4, glm::vec4> edge;


class cube : public shader::DrawObject, public rigidBody
{
public:

	cube(double inMass, double inCoeffFriction=0.5);
	void create();
	void bindVAO();
	virtual ~cube(void);

	//Derived functions from DrawObject
	virtual GLuint idxCount();
	virtual GLboolean bindIdx();
	virtual GLboolean bindPos();
	virtual GLboolean bindNor();
  virtual GLboolean bindUV();
  virtual GLboolean bindTexture();

  //Setter functions
	//scaling functions
	void setScale(float scaleX, float scaleY, float scaleZ);
  void setTexture(const char* fileName);

  //Getter functions
  glm::mat4 getScaleMatix();
	virtual glm::mat4 getModelMatrix(bool isUseTmp);
  const std::vector<glm::vec4> &getCurrentPositoins(bool isUseTmp);
  glm::vec3 getHalfDimension();
  //axisId: 0=X-axis, 1=Y-axis, 2=Z-axis
  std::vector<glm::vec4> getCurrentFace(int axisId, bool isPositiveSide, bool isUseTmp);
  //Given axisId, it will give edges parallel to that axis.
  //axisId: 0=X-axis, 1=Y-axis, 2=Z-axis
  //NOTE:: It does not consider any direction
  std::vector<edge> getCurrentAxisParallelEdge(int axisId, bool isUseTmp);

private:
	typedef std::vector<glm::vec4> face;


  ///non rigid body component///
  glm::vec3 _scale;
  //below variables for Cube-Cube Intersection Test
  std::vector<glm::vec4> positions;//This contains object space points
  std::vector<glm::vec4> updatedPos;//This contais world space points, so they are transformed of "positinos"
  //Below variable stores all face in cube as 4-verticies. It's on order, +X, +Y, +Z, -X, -Y, -Z
  //NOTE::We are not convern here of Clock wise or other way storage
  std::vector<face> faces;

  ///Shader variables
  int count;
	GLuint bufIdx;
	GLuint bufPos;
	GLuint bufNor;
  GLuint bufUV;

  GLuint textureID;

  //JUST FOR DEBUGGING PURPOSE
public:
  GLint id;

};

