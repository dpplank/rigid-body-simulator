//hare rama hare rama
//rama rama hare hare

#version 450

uniform mat4 uModelMat;
uniform mat4 uInvTranModelMat; // For normal transformation 
uniform mat4 uViewProjMat; 
uniform vec4 uColor;

in vec4 fsNormal;
in vec4 vColor;
out vec4 outColor;

void main() {

	outColor = vec4(vColor.xyz, 1.0);

}