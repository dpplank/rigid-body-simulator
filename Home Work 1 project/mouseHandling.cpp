#include "mouseHandling.h"
#include <GL/freeglut.h>
#include <iostream>
#include <math.h>

using namespace std;

//Like all staic variable, we have to define them in beginning
camera * mouseHandling::currentCamera = nullptr;
GLint mouseHandling::oldX, mouseHandling::oldY;
bool mouseHandling::isDragging;
bool mouseHandling::isZooming;
GLint mouseHandling::oldTime;
bool mouseHandling::isCtrlClick = false;
sceneStructure *mouseHandling::scene = nullptr;

mouseHandling::mouseHandling(void) {
}


mouseHandling::~mouseHandling(void) {
}

void mouseHandling::setCamera(camera* inCamera) {
	currentCamera = inCamera;
}

void mouseHandling::setScene(sceneStructure * inScene) {
  scene = inScene;
}

void mouseHandling::mouseClicking(GLint button, GLint state, GLint x, GLint y) {
		//cout<<"mouse called"<<endl;	//
  //With Ctrl click, we can define other extra functionality
  if(isCtrlClick) {
    if(button == GLUT_LEFT_BUTTON && state == GLUT_DOWN) {
      if(scene != nullptr) {
        scene->shootSphere(currentCamera->getPosition(), currentCamera->getLookAtDir());
      }
    }
  } else {
	  if(button == GLUT_LEFT_BUTTON && state == GLUT_DOWN) {
		  oldX = x;
		  oldY = y;
		  isDragging = true;
	  }

	  if(button == GLUT_LEFT_BUTTON && state == GLUT_UP) {
		  isDragging = false;
	  }

	  if(button == GLUT_RIGHT_BUTTON && state == GLUT_DOWN) {
		  oldX = x;
		  oldY = y;
		  isZooming = true;
		  oldTime = glutGet(GLUT_ELAPSED_TIME);
	  }

	  if(button == GLUT_RIGHT_BUTTON && state == GLUT_UP) {
		  isZooming = false;
	  }

  }
}

void mouseHandling::mouseDragging(GLint x, GLint y) {
		//cout<<"Draging Called"<<endl;	//
	//cout<<isDraging<<endl;	//
	if(isDragging) {
		int dX, dY;
		dX = x - oldX;
		dY = y - oldY;
		currentCamera->theta -= dX;
		currentCamera->phi += dY;

		oldX = x;
		oldY = y;
		//cout<<"OldX: "<<oldX;	//
		//cout<<"OldY: "<<oldY;	//
	}
	if(isZooming) {
		int dX, dY;
		dX = x - oldX;
		dY = y - oldY;

		//calculating dT
		int dT, t;
		t = glutGet(GLUT_ELAPSED_TIME);
		dT = t-oldTime;

    if(dT < 10) {
      dT = 10;
    }

		//Calculation speed
		float speedX = (dX>0 ? dX : -dX)/(float)dT;
		float speedY = (dY>0 ? dY : -dY)/(float)dT;
		//Limiting speed between [0.1, 1]
		speedX = (speedX<0.1 ? 0.1 : (speedX>1 ? 1 : speedX));
		speedY = (speedY<0.1 ? 0.1 : (speedY>1 ? 1 : speedY));

		currentCamera->zoom += -1 * dX * 2 * speedX;
		currentCamera->zoom +=  dY * 2 * speedY;

		oldX = x;
		oldY = y;

	}
	glutPostRedisplay();

}

void mouseHandling::setCtrlClick(bool val) {
  isCtrlClick = val;
}

bool mouseHandling::getCtrlClick() {
  return isCtrlClick;
}
