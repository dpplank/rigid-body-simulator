#pragma once
#include <glm\glm.hpp>
#include <vector>
#include <set>
#include "rigidBody.h"

#define PI 3.141592653589f
#define CHECK_OGL_ERROR utils::checkOpenglError(__FILE__, __LINE__);

typedef struct tmpStateCache {glm::vec4 xTmp; glm::mat3 RTmp;} tmpStateCache;


class utils
{
public:
	utils(void);
	~utils(void);

	static glm::mat4 mat3ToMat4 (glm::mat3 mat);
	static glm::mat3 vecCrossVecTranspose(glm::vec3 vec);
	// give equivalend vector* matrix
	static glm::mat3 giveVecStar(glm::vec3 v);

	static glm::vec4 velocityAtPoint(rigidBody *body, glm::vec4 point, bool isUseTmp);

  static bool epsilonCheck(float lhs, float rhs);

  static float getRandomNo(float min, float max);
  
  static void checkOpenglError(char *file,int line);

  static glm::vec3 getRotation(glm::vec3 vec, glm::vec3 axis, float angleInDegree);

  static void topologicalSort(std::set<rigidBody*> &nodes, std::vector<std::pair<rigidBody *, rigidBody *>> &edges, std::vector< std::vector<rigidBody*> > &outNodes);

  static float setPrecision(float num, int precision);

  static glm::mat3 giveRodrignesRot(float angle, glm::vec3 axis);


  static void fillTmpStates(std::vector<tmpStateCache>& states, rigidBody *b, int index, float dt);
};

