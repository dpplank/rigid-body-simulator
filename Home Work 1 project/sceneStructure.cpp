#include "sceneStructure.h"
#include "ground.h"
#include "sphere.h"
#include "cube.h"
#include "utils.h"
#include <iostream>

const char *groundImg="../textures/Ground_marble.bmp";
const char *ballImg = "../textures/SphereTexture.bmp";
const char *sideWall = "../textures/side_walls.bmp";

sceneStructure::sceneStructure(void)
{
}
sceneStructure::~sceneStructure(void) {
  //release all memory of rigid body
  for(int i=0; i<bodies.size(); i++) {
    delete bodies[i];
  }
}


void sceneStructure::renderScene(shader &renderingShader, shader::drawType drawType){

  for(int i=0; i<bodies.size(); i++) {
    renderingShader.loadModelMat(bodies[i]->getModelMatrix(false));
	  //renderingShader.loadUnifColor(bodies[i]->getColor());

    //Cast according to which type it represent, Tried direct method but not worked
    //Welcom to use any direct method in future
    shader::DrawObject *drawObj;
    bool onlySphere = true;

    switch(bodies[i]->getType()) {
    case (rigidBody::CUBE) : {
      drawObj = static_cast<cube *>(bodies[i]);
      break;
    } case(rigidBody::SPHERE) : {
      drawObj = static_cast<sphere *>(bodies[i]);
      onlySphere = true;
      break;
    } case(rigidBody::GROUND) : {
      drawObj = static_cast<ground *>(bodies[i]);
      break;
    } default : {
      drawObj = nullptr;
      break;
    }
    }
    if((drawObj != nullptr) &&  onlySphere)
      renderingShader.draw(*drawObj, drawType);
  }
	////load 2nd sphere
	//renderingShader.loadUnifColor(glm::vec4(0, 1, 0, 1));
	//renderingShader.loadModelMat(s2.getModelMatrix());
	//renderingShader.draw(s2);
}
  
//copy all the scene in provided vector
void sceneStructure::copyAllbodies(std::vector<rigidBody*> &destBodies) {
  for(int i=0; i<bodies.size(); i++) {
    destBodies.push_back( bodies[i]);
  }

  //
  //cube *referenceCube = new cube(1);
  //referenceCube->create();
  //referenceCube->setScale(1, 20, 1);
  //referenceCube->x = glm::vec4(0, 20, 0, 1);
  //referenceCube->setTexture(ballImg);
  //bodies.push_back(referenceCube);

  //cube *referenceCube1 = new cube(1);
  //referenceCube1->create();
  //referenceCube1->setScale(1, 20, 1);
  //referenceCube1->x = glm::vec4(-28, 20, 0, 1);
  //referenceCube1->setTexture(ballImg);
  //bodies.push_back(referenceCube1);

}


void sceneStructure::shootSphere(glm::vec4 pos, glm::vec4 direction) {
  sphere *s = new sphere(2);
  s->create();
  const float SPEED = 150;

  s->x = pos;
  s->v = direction * SPEED;
  s->force = glm::vec4(0,-9.8,0,0)*s->getMass();
  s->setRadius(8);
  s->setColor(glm::vec4(1,0,0,1));

  bodies.push_back(s);
  mainSimulation->bodies.push_back(s);
}

void sceneStructure::setSimulation(simulation *inSim) {
  mainSimulation = inSim;
}

void createStack(std::vector<rigidBody *> &bodies);

void sceneStructure::createScene() {
  //domenoesTypeCubes();
  dominoesWithCubeEnd();
  //proceduralDomenoes();
  //edgeTestCube();
  //proceduralTower();
  //blastProblem();

  	/*
	//First cube
	c1.create();
	c1.x		=	glm::vec4(0, 100,	 0, 1); //initial position
	///gravity force
	c1.applyForceTorque(glm::vec3(0, -9.8, 0)*(float)c1.getMass(), glm::vec3(0,0,0)); 
	c1.force	+=	glm::vec4(2, 0, 0, 0);
	///Apply force of (100,0,0) Newton at (-1,0,1)
	c1.applyForceTorque(glm::vec3(100,0,0), glm::vec3(-1,0,1));
	c1.setScale(10, 10, 10);
	mainSimulation.addBody(&c1);

	//Second cube
	c2.create();
	c2.force = c2.getMass() * glm::vec4(0, -9.8, 0, 0); //mg, force due to greavity
	c2.x		=	glm::vec4 (15, 110, 10, 1);
	c2.setScale(5, 5, 10);
	mainSimulation.addBody(&c2);
	*/

	//First ground
	//ground.create();
	//ground.setScale(100, 0.1, 50);
	
  //TO LOOK: AS FOR NORMAL FOR COLLISION DETECTOIN
  // I THINK ROTAION IS GIVING INVERSE RESULT, CHECK IT
  //UPDATE: I think it is right wall
  //Left Wall
  //ground *rightWall = new ground();
  //rightWall->create();
  //rightWall->setTexture(sideWall);
  //rightWall->setDimension(10, 100);
  //rightWall->setColor(glm::vec4(1, 0, 0, 1));
  //rightWall->setRotation(glm::vec3(0, 0, 1), 90); //Equivalent of rotating (0,1,0)
  //rightWall->x = glm::vec4(100, 10, 0, 1);
  //bodies.push_back(rightWall);

  //Right Wall
  //ground *leftWall = new ground();
  //leftWall->create();
  //leftWall->setTexture(sideWall);
  //leftWall->setDimension(10, 100);
  //leftWall->setColor(glm::vec4(0.44, 0.28, 0.13, 1));
  //leftWall->setRotation(glm::vec3(0, 0, 1), -90);
  //leftWall->x = glm::vec4(-100, 10, 0, 1);
  //bodies.push_back(leftWall);

  //Back wall
  //ground *backWall = new ground();
  //backWall->create();
  //backWall->setTexture(sideWall);  
  //backWall->setDimension(100, 10);
  //backWall->setColor(glm::vec4(0.44, 0.28, 0.13, 1));
  //backWall->setRotation(glm::vec3(1, 0, 0), 90);
  //backWall->x = glm::vec4(0, 10, -100, 1);
  //bodies.push_back(backWall);

  //Front wall
  //ground *frontWall = new ground();
  //frontWall->create();
  //frontWall->setTexture(sideWall);
  //frontWall->setDimension(100, 10);
  //frontWall->setColor(glm::vec4(0.44, 0.28, 0.13, 1));
  //frontWall->setRotation(glm::vec3(1, 0, 0), -90);
  //frontWall->x = glm::vec4(0, 10, 100, 1);
  //bodies.push_back(frontWall);

  ///Random spheres Generation
  //for(int i=0; i<100; i++) {
  //  float radius = utils::getRandomNo(5,15);
  //  sphere *s = new sphere(radius * 2);
	 // s->create();
	 // s->setScale(radius, radius, radius);
  //	s->x = glm::vec4(utils::getRandomNo(-90,90), i+20, utils::getRandomNo(-90,90), 1);
  //	s->force = glm::vec4(0, -9.8, 0, 0)*s->getMass();
  //	s->v = glm::vec4(utils::getRandomNo(-20,20), utils::getRandomNo(-20,20), utils::getRandomNo(-20,20), 0);
  //  s->setColor(glm::vec4(utils::getRandomNo(0,1), utils::getRandomNo(0,1), utils::getRandomNo(0,1), 1));
  //  bodies.push_back(s);
  //}
  //system("pause");

  ///Manual Spheres Generation

 // sphere *s1 = new sphere(2);
 // s1->create();
 // s1->setTexture(ballImg);
 // s1->setScale(3,3,3);
 // s1->x = glm::vec4(-3, 3, 0, 1);
 // s1->force = glm::vec4(0, -9.8, 0, 0)*s1->getMass();
 // bodies.push_back(s1);

 // sphere *s2 = new sphere(2);
 // s2->create();
 // s2->setTexture(ballImg);
 // s2->setScale(3,3,3);
 // s2->x = glm::vec4(3, 3, 0, 1);
 // s2->force = glm::vec4(0, -9.8, 0, 0)*s2->getMass();
 // bodies.push_back(s2);
 // 
 // sphere *s3 = new sphere(2);
 // s3->create();
 // s3->setTexture(ballImg);
 // s3->setScale(3,3,3);
 // s3->x = glm::vec4(9, 3, 0, 1);
 // s3->force = glm::vec4(0, -9.8, 0, 0)*s3->getMass();
 // bodies.push_back(s3);

 // sphere *s4 = new sphere(2);
 // s4->create();
 // s4->setTexture(ballImg);
 // s4->setScale(3,3,3);
 // s4->x = glm::vec4(15, 3, 0, 1);
 // s4->force = glm::vec4(0, -9.8, 0, 0)*s4->getMass();
 // bodies.push_back(s4);

  //sphere *s4 = new sphere(2);
  //s4->create();
  //s4->setTexture(ballImg);
  //s4->setScale(3,3,3);
  //s4->x = glm::vec4(2, 25, 2, 1);
  //s4->force = glm::vec4(0, -9.8, 0, 0)*s4->getMass();
  //bodies.push_back(s4);

  //sphere *s5 = new sphere(2);
  //s5->create();
  //s5->setTexture(ballImg);
  //s5->setScale(3,3,3);
  //s5->x = glm::vec4(2, 5, 2, 1);
  //s5->force = glm::vec4(0, -9.8, 0, 0)*s5->getMass();
  //bodies.push_back(s5);

  //cube *c = new cube(5);
  //c->create();
  //c->setTexture(sideWall);  
  //c->setScale(0.1, 1, 1);
  //c->force = glm::vec4(0, -9.8, 0, 0)*c->getMass();
  //c->x = glm::vec4(0, 3, 0, 1);
  //c->omega = glm::vec3(1,0,0.1);
  //bodies.push_back(c);
  
  
  //createStack(bodies);

}

void sceneStructure::dominoesWithCubeEnd() {
  ground *g = new ground();
	g->create();
  g->x = glm::vec4(0, -0.1, 0, 1);
  g->setTexture(groundImg);
	g->setDimension(100, 100);
  g->setColor(glm::vec4(0.48, 0.32, 0.19, 1));
  bodies.push_back(g);


  sphere *s = new sphere(1);
	s->create();
  s->setTexture(ballImg);
	s->setScale(3, 3, 3);
	s->x = glm::vec4(45, 6, 0, 1);
	s->force = glm::vec4(0, -9.8, 0, 0)*s->getMass();
	s->v = glm::vec4(-10, 0, 0, 0); 
  s->omega = glm::vec3(0, 0, 0);
  s->setColor(glm::vec4(0, 1, 1, 1));
  bodies.push_back(s);

  int idNo=1;

  cube *c1 = new cube(5);
  c1->create();
  c1->setTexture(sideWall);  
  c1->setScale(5, 5, 5);
  c1->force = glm::vec4(0, -9.8, 0, 0)*c1->getMass();
  c1->x = glm::vec4(0, 10, 0, 1);
  //c1->omega = glm::vec3(15,0,0);
  c1->id = idNo++;
  //bodies.push_back(c1);

  cube *c2 = new cube(5);
  c2->create();
  c2->setTexture(sideWall);  
  c2->setScale(.5, 5, 5);
  c2->force = glm::vec4(0, -9.8, 0, 0)*c2->getMass();
  c2->x = glm::vec4(6, 10, 0, 1);
  //c1->omega = glm::vec3(15,0,0);
  c2->id = idNo++;
  //bodies.push_back(c2);

  cube *c3 = new cube(5);
  c3->create();
  c3->setTexture(sideWall);  
  c3->setScale(.5, 5, 5);
  c3->force = glm::vec4(0, -9.8, 0, 0)*c3->getMass();
  c3->x = glm::vec4(12, 10, 0, 1);
  //c1->omega = glm::vec3(15,0,0);
  c3->id = idNo++;
  //bodies.push_back(c3);

  cube *c4 = new cube(5);
  c4->create();
  c4->setTexture(sideWall);  
  c4->setScale(.5, 5, 5);
  c4->force = glm::vec4(0, -9.8, 0, 0)*c4->getMass();
  c4->x = glm::vec4(18, 10, 0, 1);
  //c1->omega = glm::vec3(15,0,0);
  c4->id = idNo++;
  //bodies.push_back(c4);

  cube *c5 = new cube(5);
  c5->create();
  c5->setTexture(sideWall);  
  c5->setScale(.5, 5, 5);
  c5->force = glm::vec4(0, -9.8, 0, 0)*c5->getMass();
  c5->x = glm::vec4(24, 10, 0, 1);
  //c1->omega = glm::vec3(15,0,0);
  c5->id = idNo++;
  //bodies.push_back(c5);

  bodies.push_back(c5);
  bodies.push_back(c3);
  bodies.push_back(c1);
  bodies.push_back(c2);
  bodies.push_back(c4);


}

void sceneStructure::twoSimpleCube() {
  ground *g = new ground();
	g->create();
  g->x = glm::vec4(0, -0.1, 0, 1);
  g->setTexture(groundImg);
	g->setDimension(100, 100);
  g->setColor(glm::vec4(0.48, 0.32, 0.19, 1));
  bodies.push_back(g);

  int idNo=1;

  cube *c = new cube(5);
  c->create();
  c->setTexture(sideWall);  
  c->setScale(5, 5, 5);
  c->force = glm::vec4(0, -9.8, 0, 0)*c->getMass();
  c->x = glm::vec4(0, 10, 0, 1);
  //c1->omega = glm::vec3(15,0,0);
  c->id = idNo++;
  bodies.push_back(c);

  //c = new cube(5);
  //c->create();
  //c->setTexture(sideWall);  
  //c->setScale(5, 5, 5);
  //c->force = glm::vec4(0, -9.8, 0, 0)*c->getMass();
  //c->x = glm::vec4(0, 21, 0, 1);
  ////c1->omega = glm::vec3(15,0,0);
  //c->id = idNo++;
  //bodies.push_back(c);


}

void sceneStructure::domenoesTypeCubes() {
  ground *g = new ground();
	g->create();
  g->x = glm::vec4(0, -0.1, 0, 1);
  g->setTexture(groundImg);
	g->setDimension(100, 100);
  g->setColor(glm::vec4(0.48, 0.32, 0.19, 1));
  bodies.push_back(g);

 // sphere *s = new sphere(1);
	//s->create();
 // s->setTexture(ballImg);
	//s->setScale(3, 3, 3);
	//s->x = glm::vec4(45, 6, 0, 1);
	//s->force = glm::vec4(0, -9.8, 0, 0)*s->getMass();
	//s->v = glm::vec4(-10, 0, 0, 0); 
 // s->omega = glm::vec3(0, 0, 0);
 // s->setColor(glm::vec4(0, 1, 1, 1));
 // bodies.push_back(s);

  int idNo=1;

  cube *c1 = new cube(5);
  c1->create();
  c1->setTexture(sideWall);  
  c1->setScale(5, .5, 5);
  c1->force = glm::vec4(0, -9.8, 0, 0)*c1->getMass();
  c1->x = glm::vec4(-6, 10, 0, 1);
  c1->omega = glm::vec3(0,70,0);
  //c1->setRotation(glm::vec3(0,0,1), 90);
  c1->id = idNo++;
  bodies.push_back(c1);


  c1 = new cube(5);
  c1->create();
  c1->setTexture(sideWall);  
  c1->setScale(.5, 5, 5);
  c1->force = glm::vec4(0, -9.8, 0, 0)*c1->getMass();
  c1->x = glm::vec4(0, 10, 0, 1);
  c1->omega = glm::vec3(0,0,15);
  c1->id = idNo++;
  bodies.push_back(c1);


}

void sceneStructure::proceduralDomenoes() {
  ground *g = new ground();
	g->create();
  g->x = glm::vec4(0, -0.1, 0, 1);
  g->setTexture(groundImg);
	g->setDimension(100, 100);
  g->setColor(glm::vec4(0.48, 0.32, 0.19, 1));
  bodies.push_back(g);

  sphere *s = new sphere(1);
	s->create();
  s->setTexture(ballImg);
	s->setScale(3, 3, 3);
	s->x = glm::vec4(45, 6, 0, 1);
	s->force = glm::vec4(0, -9.8, 0, 0)*s->getMass();
	s->v = glm::vec4(-10, 0, 0, 0); 
  s->omega = glm::vec3(0, 0, 0);
  s->setColor(glm::vec4(0, 1, 1, 1));
  bodies.push_back(s);

  int idNo=1;
  cube *c;
  for(int i=20; i>=0; i--) {
    c = new cube(5);
    c->create();
    c->setTexture(sideWall);  
    c->setScale(.5, 5, 5);
    c->force = glm::vec4(0, -9.8, 0, 0)*c->getMass();
    c->x = glm::vec4(-i*6, 10, 0, 1);
    c->omega = glm::vec3(15,0,0);
    c->id = idNo++;
    bodies.push_back(c);
  }

}

void sceneStructure::edgeTestCube() {
  ground *g = new ground();
	g->create();
  g->x = glm::vec4(0, -0.1, 0, 1);
  g->setTexture(groundImg);
	g->setDimension(100, 100);
  g->setColor(glm::vec4(0.48, 0.32, 0.19, 1));
  bodies.push_back(g);

  int idNo=1;

  cube *c1 = new cube(5);
  c1->create();
  c1->setTexture(sideWall);  
  c1->setScale(5, 5, 5);
  c1->force = glm::vec4(0, -9.8, 0, 0)*c1->getMass();
  c1->x = glm::vec4(0, 5, 0, 1);
  //c1->omega = glm::vec3(0,70,0);
  //c1->setRotation(glm::vec3(0,0,1), 90);
  c1->id = idNo++;
  bodies.push_back(c1);

  c1 = new cube(5);
  c1->create();
  c1->setTexture(sideWall);  
  c1->setScale(10, 5, 1);
  c1->force = glm::vec4(0, -9.8, 0, 0)*c1->getMass();
  c1->x = glm::vec4(5, 40, 0, 1);
  //c1->omega = glm::vec3(0,70,0);
  //c1->setRotation(glm::vec3(0,0,1), 90);
  c1->id = idNo++;
  bodies.push_back(c1);

}

void sceneStructure::proceduralTower() {
  ground *g = new ground();
	g->create();
  g->x = glm::vec4(0, -0.1, 0, 1);
  g->setTexture(groundImg);
	g->setDimension(100, 100);
  g->setColor(glm::vec4(0.48, 0.32, 0.19, 1));
  bodies.push_back(g);

 // sphere *s = new sphere(1);
	//s->create();
 // s->setTexture(ballImg);
	//s->setScale(3, 3, 3);
	//s->x = glm::vec4(45, 6, 0, 1);
	//s->force = glm::vec4(0, -9.8, 0, 0)*s->getMass();
	//s->v = glm::vec4(-10, 0, 0, 0); 
 // s->omega = glm::vec3(0, 0, 0);
 // s->setColor(glm::vec4(0, 1, 1, 1));
 // bodies.push_back(s);

  int idNo=1;
  cube *c;
  for(int i=1; i<10; i++) {
    c = new cube(5);
    c->create();
    c->setTexture(sideWall);  
    c->setScale(5, .5, 5);
    c->force = glm::vec4(0, -9.8, 0, 0)*c->getMass();
    c->x = glm::vec4(i*0.1, i*11, 0, 1);
    //c->omega = glm::vec3(15,0,0);
    c->id = idNo++;
    bodies.push_back(c);
  }

  //for(int i=1; i<45; i++) {
  //  c = new cube(5);
  //  c->create();
  //  c->setTexture(sideWall);  
  //  c->setScale(.5, 5, 5);
  //  c->force = glm::vec4(0, -9.8, 0, 0)*c->getMass();
  //  c->x = glm::vec4(10, i*11, 0, 1);
  //  //c->omega = glm::vec3(15,0,0);
  //  c->id = idNo++;
  //  bodies.push_back(c);
  //}
  }

void sceneStructure::blastProblem() {
  ground *g = new ground();
	g->create();
  g->x = glm::vec4(0, -0.1, 0, 1);
  g->setTexture(groundImg);
	g->setDimension(100, 100);
  g->setColor(glm::vec4(0.48, 0.32, 0.19, 1));
  bodies.push_back(g);

  int idNo=1;

  cube *c1 = new cube(5);
  c1->create();
  c1->setTexture(sideWall);  
  c1->setScale(5, .5, 5);
  c1->force = glm::vec4(0, -9.8, 0, 0)*c1->getMass();
  c1->x = glm::vec4(-6, 1, 0, 1);
  //c1->omega = glm::vec3(0,70,0);
  //c1->setRotation(glm::vec3(0,0,1), 90);
  c1->id = idNo++;
  bodies.push_back(c1);


  c1 = new cube(5);
  c1->create();
  c1->setTexture(sideWall);  
  c1->setScale(5, .5, 5);
  c1->force = glm::vec4(0, -9.8, 0, 0)*c1->getMass();
  c1->x = glm::vec4(0, 2, 0, 1);
  c1->omega = glm::vec3(0,0,15);
  c1->id = idNo++;
  bodies.push_back(c1);

  c1 = new cube(5);
  c1->create();
  c1->setTexture(sideWall);  
  c1->setScale(.5, 5, 5);
  c1->force = glm::vec4(0, -9.8, 0, 0)*c1->getMass();
  c1->x = glm::vec4(10, 10, 0, 1);
  c1->omega = glm::vec3(0,0,15);
  c1->id = idNo++;
  bodies.push_back(c1);

}

void createStack(std::vector<rigidBody *> &bodies) {
  cube *c1 = new cube(5);
  c1->create();
  c1->setTexture(sideWall);  
  c1->setScale(5, 5, 5);
  c1->force = glm::vec4(0, -9.8, 0, 0)*c1->getMass();
  c1->x = glm::vec4(0, 11, 0, 1);
  //c1->omega = glm::vec3(15,0,0);
  bodies.push_back(c1);

  c1 = new cube(5);
  c1->create();
  c1->setTexture(sideWall);  
  c1->setScale(5, 5, 5);
  c1->force = glm::vec4(0, -9.8, 0, 0)*c1->getMass();
  c1->x = glm::vec4(0, 22, 0, 1);
  //c1->omega = glm::vec3(15,0,0);
  bodies.push_back(c1);

  c1 = new cube(5);
  c1->create();
  c1->setTexture(sideWall);  
  c1->setScale(5, 5, 5);
  c1->force = glm::vec4(0, -9.8, 0, 0)*c1->getMass();
  c1->x = glm::vec4(0, 33, 0, 1);
  //c1->omega = glm::vec3(15,0,0);
  bodies.push_back(c1);

  c1 = new cube(5);
  c1->create();
  c1->setTexture(sideWall);  
  c1->setScale(5, 5, 5);
  c1->force = glm::vec4(0, -9.8, 0, 0)*c1->getMass();
  c1->x = glm::vec4(0, 44, 0, 1);
  //c1->omega = glm::vec3(15,0,0);
  bodies.push_back(c1);

  c1 = new cube(5);
  c1->create();
  c1->setTexture(sideWall);  
  c1->setScale(5, 5, 5);
  c1->force = glm::vec4(0, -9.8, 0, 0)*c1->getMass();
  c1->x = glm::vec4(0, 55, 0, 1);
  //c1->omega = glm::vec3(15,0,0);
  bodies.push_back(c1);

  c1 = new cube(5);
  c1->create();
  c1->setTexture(sideWall);  
  c1->setScale(5, 5, 5);
  c1->force = glm::vec4(0, -9.8, 0, 0)*c1->getMass();
  c1->x = glm::vec4(0, 66, 0, 1);
  //c1->omega = glm::vec3(15,0,0);
  bodies.push_back(c1);

  for(int i=7; i<22; i++) {
    c1 = new cube(5);
    c1->create();
    c1->setTexture(sideWall);  
    c1->setScale(5, 5, 5);
    c1->force = glm::vec4(0, -9.8, 0, 0)*c1->getMass();
    c1->x = glm::vec4(0, i*11, 0, 1);
    //c1->omega = glm::vec3(15,0,0);
    bodies.push_back(c1);

  }

}

