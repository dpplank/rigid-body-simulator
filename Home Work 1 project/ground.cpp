#include "ground.h"
#include "utils.h"

ground::ground(void) : cube(-1) { //-1 for infinite mass
	setScale(5, 0.1, 5);
	Ibodyinv = glm::mat3(0); //3x3 zero matrix
  v = glm::vec4(0,0,0,0);
  omega = glm::vec3(0,0,0);
	bodyType = rigidBody::GROUND;

}


ground::~ground(void)
{
}

void ground::setDimension(float width, float height) {
	setScale(width, 0.1, height);
}

glm::vec3 ground::getNormal() {
	//transform (0.0, 1.0, 0.0, 0.0) with appropriate matrix called G
	//as we know G = (M-1)T or Transpose(inverse(M)), and if M is orthgonal like rotational
	//then inverse(M) is Transpose(M) so G = M;
	return getRotationMatrix(false) * glm::vec3(0,1,0);	
}

glm::vec4 ground::getPointOnPlane() {
	//giving (1,1,1) point in new transfored position
	return getModelMatrix() * glm::vec4(1.0, 1.0, 1.0, 1.0);
}

glm::mat4 ground::getModelMatrix() {
  return giveCurrentPosMatrix(false) * utils::mat3ToMat4(getRotationMatrix(false)) * getScaleMatix();

}
