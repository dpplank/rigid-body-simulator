/*in vec4 vPosition;
varying vec4 Cooolor;

void
main() {
	gl_Position=vPosition;
	Cooolor=vec4((1.0+vPosition.xyz)/2.0,1.0);
}*/

#version 440

uniform mat4 uViewProjMat;

in vec4 vsPosition;

out vec4 color;

void main(){
	gl_Position=uViewProjMat * vsPosition;

	color=vec4(1.0,0.0,0.0,1.0);
}