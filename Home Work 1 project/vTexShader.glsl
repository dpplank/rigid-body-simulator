/*Hare Krishna Hare Krishna 
Krishna Krishna Hare Hare */

#version 450

//in stream data
in vec4 vsPosition;
in vec4 vsNormal;
in vec2 vsUV;

//Uniform Variables
uniform mat4 uModelMat;
uniform mat4 uInvTranModelMat; //for normal transformation
uniform mat4 uViewProjMat;
uniform vec4 uColor;

//out stream data
out vec4 fsNormal; //Passing to fragment shader
out vec4 vColor; //debugging
out vec2 fsUV;

void main() {
  vColor = uColor;

  //Just passing uv to fragment shader
  fsUV = vsUV;

  //Dealing with position
  vec4 worldPos = uModelMat * vsPosition;

  gl_Position = uViewProjMat * worldPos;
}